-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 12, 2023 at 05:46 AM
-- Server version: 10.6.10-MariaDB-cll-lve
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u908543517_kanime`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_cmt_reply`
--

CREATE TABLE `tb_cmt_reply` (
  `id` int(11) NOT NULL,
  `content` varchar(9999) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_cmt_reply`
--

INSERT INTO `tb_cmt_reply` (`id`, `content`, `comment_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(55, ':(', 95, 22, '2022-12-17 15:13:55', '2022-12-17 15:13:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_comment`
--

CREATE TABLE `tb_comment` (
  `id` int(11) NOT NULL,
  `content` varchar(9999) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `movie_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_comment`
--

INSERT INTO `tb_comment` (`id`, `content`, `user_id`, `movie_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(92, '=))', 1, 54, '2022-12-16 20:58:43', '2022-12-16 20:58:43', NULL),
(93, 'Gà vl', 2, 54, '2022-12-16 21:13:37', '2022-12-16 21:13:37', NULL),
(94, 'Chuối vl', 2, 54, '2022-12-16 21:13:51', '2022-12-16 21:13:51', NULL),
(95, 'Tôi đã cảm lạnh khi xem phim này :>', 22, 46, '2022-12-17 15:13:36', '2022-12-17 15:13:36', NULL),
(96, '2`', 22, 1, '2022-12-17 15:17:56', '2022-12-17 15:17:56', NULL),
(97, 'Hay quá', 1, 53, '2023-01-07 08:33:43', '2023-01-07 08:33:43', NULL),
(98, 'alo', 23, 22, '2023-01-07 20:46:00', '2023-01-07 20:46:00', NULL),
(99, 'oke', 22, 22, '2023-01-07 20:46:34', '2023-01-07 20:46:34', NULL),
(100, 'haha', 25, 22, '2023-01-10 20:55:12', '2023-01-10 20:55:12', NULL),
(101, 'hihi', 25, 5, '2023-01-10 21:32:51', '2023-01-10 21:32:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_country`
--

CREATE TABLE `tb_country` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_country`
--

INSERT INTO `tb_country` (`id`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Trung Quốc', 'trung-quoc', '2022-10-29 10:54:55', '2022-10-29 10:54:55', NULL),
(2, 'Hàn Quốc', 'han-quoc', '2022-10-29 10:55:07', '2022-10-29 10:55:07', NULL),
(3, 'Nhật Bản', 'nhat-ban', '2022-10-29 10:55:13', '2022-10-29 10:55:13', NULL),
(4, 'Thái Lan', 'thai-lan', '2022-10-29 10:55:25', '2022-10-29 10:55:25', NULL),
(5, 'Hồng Kông', 'hong-kong', '2022-10-29 10:55:39', '2022-10-29 10:55:39', NULL),
(6, 'Mỹ', 'my', '2022-10-29 10:55:52', '2022-10-29 10:55:52', NULL),
(7, 'Châu Âu', 'chau-au', '2022-10-29 10:55:59', '2022-10-29 10:55:59', NULL),
(8, 'Ấn Độ', 'an-do', '2022-10-29 10:56:06', '2022-10-29 10:56:06', NULL),
(9, 'Pháp', 'phap', '2022-10-29 10:56:18', '2022-10-29 10:56:18', NULL),
(10, 'Anh', 'anh', '2022-10-29 10:56:25', '2022-10-29 10:56:25', NULL),
(11, 'Canada', 'canada', '2022-10-29 10:56:31', '2022-10-29 10:56:31', NULL),
(12, 'Đức', 'duc', '2022-10-29 10:56:41', '2022-10-29 10:56:41', NULL),
(13, 'Tây Ban Nha', 'tay-ban-nha', '2022-10-29 10:56:51', '2022-10-29 10:56:51', NULL),
(14, 'Nga', 'nga', '2022-10-29 10:57:07', '2022-10-29 10:57:07', NULL),
(15, 'Úc', 'uc', '2022-10-29 10:57:12', '2022-10-29 10:57:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_episode`
--

CREATE TABLE `tb_episode` (
  `id` int(11) NOT NULL,
  `episode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hls` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `movie_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_episode`
--

INSERT INTO `tb_episode` (`id`, `episode`, `hls`, `movie_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', 'https://kd.hd-bophim.com/20220818/20264_c99d4d76/index.m3u8', 1, '2022-10-29 11:13:28', '2022-10-29 11:13:44', NULL),
(2, '2', 'https://1080.hdphimonline.com/20221225/40004_cb8766d6/index.m3u8', 1, '2022-10-29 11:17:01', '2022-12-31 14:37:58', NULL),
(3, '9-end', 'https://hd.hdbophim.com/20221013/23816_2390992d/index.m3u8', 1, '2022-10-29 11:52:23', '2022-10-29 11:52:23', NULL),
(4, '8', 'https://hd.1080phim.com/20221006/27117_e7fc745a/index.m3u8', 1, '2022-10-29 11:52:24', '2022-10-29 11:52:24', NULL),
(5, '7', 'https://hd.hdbophim.com/20220929/22981_f1345446/index.m3u8', 1, '2022-10-29 11:52:24', '2022-10-29 11:52:24', NULL),
(6, '6', 'https://hd.hdbophim.com/20220922/22611_80ecac17/index.m3u8', 1, '2022-10-29 11:52:25', '2022-10-29 11:52:25', NULL),
(7, '5', 'https://1080.hdphimonline.com/20221225/40002_ba16b9aa/index.m3u8', 1, '2022-10-29 11:52:25', '2022-12-31 14:39:36', NULL),
(8, '4', 'https://1080.hdphimonline.com/20221225/40006_6c8e27b7/index.m3u8', 1, '2022-10-29 11:52:25', '2022-12-31 14:39:10', NULL),
(9, '3', 'https://1080.hdphimonline.com/20221225/40005_0f327a4a/index.m3u8', 1, '2022-10-29 11:52:26', '2022-12-31 14:38:43', NULL),
(10, '10-end', 'https://1080.hdphimonline.com/20221022/36906_cd11fad9/index.m3u8', 2, '2022-10-29 12:50:10', '2022-10-29 12:50:10', NULL),
(11, '9', 'https://hd.1080phim.com/20221017/27405_01745ee8/index.m3u8', 2, '2022-10-29 12:50:10', '2022-10-29 12:50:10', NULL),
(12, '8', 'https://hd.hdbophim.com/20221010/23606_3930118b/index.m3u8', 2, '2022-10-29 12:54:08', '2022-10-29 12:54:08', NULL),
(13, '7', 'https://hd.1080phim.com/20221003/26952_4b843890/index.m3u8', 2, '2022-10-29 12:54:08', '2022-12-31 14:43:20', NULL),
(14, '6', 'https://hd.1080phim.com/20221219/30060_df34b0b0/index.m3u8', 2, '2022-10-29 12:54:08', '2022-12-31 14:43:10', NULL),
(15, '5', 'https://hd.1080phim.com/20221219/30059_d61d625c/index.m3u8', 2, '2022-10-29 12:54:08', '2022-12-31 14:42:53', NULL),
(16, '4', 'https://hd.1080phim.com/20221219/30058_80f06598/index.m3u8', 2, '2022-10-29 12:54:08', '2022-12-31 14:42:41', NULL),
(17, '3', 'https://hd.1080phim.com/20221219/30057_fec4fbab/index.m3u8', 2, '2022-10-29 12:54:08', '2022-12-31 14:42:31', NULL),
(18, '2', 'https://hd.1080phim.com/20221219/30056_fe082eef/index.m3u8', 2, '2022-10-29 12:54:08', '2022-12-31 14:42:13', NULL),
(19, '1', 'https://hd.1080phim.com/20221219/30055_5a02ba89/index.m3u8', 2, '2022-10-29 12:54:08', '2022-12-31 14:41:55', NULL),
(20, '8-end', 'https://kd.hd-bophim.com/20221014/24408_be36e35c/index.m3u8', 3, '2022-10-29 13:00:28', '2022-10-29 13:00:28', NULL),
(21, '7', 'https://hd.1080phim.com/20221007/27171_64206836/index.m3u8', 3, '2022-10-29 13:00:28', '2022-10-29 13:00:28', NULL),
(22, '6', 'https://hd.hdbophim.com/20220930/23023_a8aa93ba/index.m3u8', 3, '2022-10-29 13:00:28', '2022-10-29 13:00:28', NULL),
(23, '5', 'https://hd.hdbophim.com/20220923/22621_1ed7d34d/index.m3u8', 3, '2022-10-29 13:00:28', '2022-10-29 13:00:28', NULL),
(24, '4', 'https://kd.hd-bophim.com/20220916/21518_7eb80f5a/index.m3u8', 3, '2022-10-29 13:00:28', '2022-10-29 13:00:28', NULL),
(25, '3', 'https://kd.hd-bophim.com/20220909/21227_b76828ad/index.m3u8', 3, '2022-10-29 13:00:28', '2022-10-29 13:00:28', NULL),
(26, '2', 'https://kd.hd-bophim.com/20220902/20981_51dea64c/index.m3u8', 3, '2022-10-29 13:00:28', '2022-10-29 13:00:28', NULL),
(27, '1', 'https://kd.hd-bophim.com/20220902/20980_e6c34f94/index.m3u8', 3, '2022-10-29 13:00:28', '2022-10-29 13:00:28', NULL),
(28, '13-end', 'https://hd.hdbophim.com/20220401/5290_9cd0963f/index.m3u8', 4, '2022-10-29 13:22:52', '2022-10-29 13:22:52', NULL),
(29, '12', 'https://hd.hdbophim.com/20220401/5289_f4a574f1/index.m3u8', 4, '2022-10-29 13:22:52', '2022-10-29 13:22:52', NULL),
(30, '11', 'https://hd.hdbophim.com/20220401/5288_2075508f/index.m3u8', 4, '2022-10-29 13:22:52', '2022-10-29 13:22:52', NULL),
(31, '10', 'https://hd.hdbophim.com/20220401/5287_34ff5d6d/index.m3u8', 4, '2022-10-29 13:22:52', '2022-10-29 13:22:52', NULL),
(32, '9', 'https://hd.hdbophim.com/20220401/5286_5d3cbcff/index.m3u8', 4, '2022-10-29 13:22:52', '2022-10-29 13:22:52', NULL),
(33, '8', 'https://hd.hdbophim.com/20220401/5285_f137b65e/index.m3u8', 4, '2022-10-29 13:22:52', '2022-10-29 13:22:52', NULL),
(34, '7', 'https://hd.hdbophim.com/20220401/5284_0f7b4373/index.m3u8', 4, '2022-10-29 13:22:52', '2022-10-29 13:22:52', NULL),
(35, '6', 'https://hd.hdbophim.com/20220401/5283_4c487a92/index.m3u8', 4, '2022-10-29 13:22:52', '2022-10-29 13:22:52', NULL),
(36, '5', 'https://hd.hdbophim.com/20220401/5282_4ce6a42d/index.m3u8', 4, '2022-10-29 13:22:52', '2022-10-29 13:22:52', NULL),
(37, '4', 'https://hd.hdbophim.com/20220401/5281_088d9664/index.m3u8', 4, '2022-10-29 13:22:52', '2022-10-29 13:22:52', NULL),
(38, '3', 'https://hd.hdbophim.com/20220401/5280_5dd7c933/index.m3u8', 4, '2022-10-29 13:22:52', '2022-10-29 13:22:52', NULL),
(39, '2', 'https://hd.hdbophim.com/20220401/5279_e3d6996e/index.m3u8', 4, '2022-10-29 13:22:52', '2022-10-29 13:22:52', NULL),
(40, '1', 'https://hd.hdbophim.com/20220401/5278_934826e3/index.m3u8', 4, '2022-10-29 13:22:52', '2022-10-29 13:22:52', NULL),
(41, '9-end', 'https://hd.hdbophim.com/20220401/5299_6d68a116/index.m3u8', 5, '2022-10-29 13:26:02', '2022-10-29 13:26:02', NULL),
(42, '8', 'https://hd.hdbophim.com/20220401/5298_7d3edfe4/index.m3u8', 5, '2022-10-29 13:26:02', '2022-10-29 13:26:02', NULL),
(43, '7', 'https://hd.hdbophim.com/20220401/5297_08895f3a/index.m3u8', 5, '2022-10-29 13:26:02', '2022-10-29 13:26:02', NULL),
(44, '6', 'https://hd.hdbophim.com/20220401/5296_e30ae98d/index.m3u8', 5, '2022-10-29 13:26:02', '2022-10-29 13:26:02', NULL),
(45, '5', 'https://hd.hdbophim.com/20220401/5295_f1f9efe9/index.m3u8', 5, '2022-10-29 13:26:02', '2022-10-29 13:26:02', NULL),
(46, '4', 'https://hd.hdbophim.com/20220401/5294_e3f58769/index.m3u8', 5, '2022-10-29 13:26:02', '2022-10-29 13:26:02', NULL),
(47, '3', 'https://hd.hdbophim.com/20220401/5293_7c3ff8aa/index.m3u8', 5, '2022-10-29 13:26:02', '2022-10-29 13:26:02', NULL),
(48, '2', 'https://hd.hdbophim.com/20220401/5292_e7435683/index.m3u8', 5, '2022-10-29 13:26:02', '2022-10-29 13:26:02', NULL),
(49, '1', 'https://hd.hdbophim.com/20220401/5291_ac1c89fd/index.m3u8', 5, '2022-10-29 13:26:02', '2022-10-29 13:26:02', NULL),
(50, '8-end', 'https://hd.hdbophim.com/20220401/5307_c507c965/index.m3u8', 6, '2022-10-29 13:29:01', '2022-10-29 13:29:01', NULL),
(51, '7', 'https://hd.hdbophim.com/20220401/5306_7f4d96ec/index.m3u8', 6, '2022-10-29 13:29:01', '2022-10-29 13:29:01', NULL),
(52, '6', 'https://hd.hdbophim.com/20220401/5305_751215d3/index.m3u8', 6, '2022-10-29 13:29:01', '2022-10-29 13:29:01', NULL),
(53, '5', 'https://hd.hdbophim.com/20220401/5304_b4030388/index.m3u8', 6, '2022-10-29 13:29:01', '2022-10-29 13:29:01', NULL),
(54, '4', 'https://hd.hdbophim.com/20220401/5303_a3feeb47/index.m3u8', 6, '2022-10-29 13:29:01', '2022-10-29 13:29:01', NULL),
(55, '3', 'https://hd.hdbophim.com/20220401/5302_3ed34ded/index.m3u8', 6, '2022-10-29 13:29:01', '2022-10-29 13:29:01', NULL),
(56, '2', 'https://hd.hdbophim.com/20220401/5301_1aee44f4/index.m3u8', 6, '2022-10-29 13:29:01', '2022-10-29 13:29:01', NULL),
(57, '1', 'https://hd.hdbophim.com/20220401/5300_2d617870/index.m3u8', 6, '2022-10-29 13:29:01', '2022-10-29 13:29:01', NULL),
(58, '8-end', 'https://hd.hdbophim.com/20220401/5315_62935932/index.m3u8', 7, '2022-10-29 13:32:03', '2022-10-29 13:32:03', NULL),
(59, '7', 'https://hd.hdbophim.com/20220401/5314_807e7699/index.m3u8', 7, '2022-10-29 13:32:03', '2022-10-29 13:32:03', NULL),
(60, '6', 'https://hd.hdbophim.com/20220401/5313_85bca2e5/index.m3u8', 7, '2022-10-29 13:32:03', '2022-10-29 13:32:03', NULL),
(61, '5', 'https://hd.hdbophim.com/20220401/5312_89cbc80c/index.m3u8', 7, '2022-10-29 13:32:03', '2022-10-29 13:32:03', NULL),
(62, '4', 'https://hd.hdbophim.com/20220401/5311_b467aea5/index.m3u8', 7, '2022-10-29 13:32:03', '2022-10-29 13:32:03', NULL),
(63, '3', 'https://hd.hdbophim.com/20220401/5310_5a6708e3/index.m3u8', 7, '2022-10-29 13:32:03', '2022-10-29 13:32:03', NULL),
(64, '2', 'https://hd.hdbophim.com/20220401/5309_ad391229/index.m3u8', 7, '2022-10-29 13:32:03', '2022-10-29 13:32:03', NULL),
(65, '1', 'https://hd.hdbophim.com/20220401/5308_ed1f5c7a/index.m3u8', 7, '2022-10-29 13:32:03', '2022-10-29 13:32:03', NULL),
(66, '10-end', 'https://hd.hdbophim.com/20220401/5325_74bdfd15/index.m3u8', 8, '2022-10-29 13:34:08', '2022-10-29 13:34:08', NULL),
(67, '9', 'https://hd.hdbophim.com/20220401/5324_92f893b9/index.m3u8', 8, '2022-10-29 13:34:08', '2022-10-29 13:34:08', NULL),
(68, '8', 'https://hd.hdbophim.com/20220401/5323_63c3f5a6/index.m3u8', 8, '2022-10-29 13:34:08', '2022-10-29 13:34:08', NULL),
(69, '7', 'https://hd.hdbophim.com/20220401/5322_10f9dbda/index.m3u8', 8, '2022-10-29 13:34:08', '2022-10-29 13:34:08', NULL),
(70, '6', 'https://hd.hdbophim.com/20220401/5321_a6753251/index.m3u8', 8, '2022-10-29 13:34:08', '2022-10-29 13:34:08', NULL),
(71, '5', 'https://hd.hdbophim.com/20220401/5320_4a4336b5/index.m3u8', 8, '2022-10-29 13:34:08', '2022-10-29 13:34:08', NULL),
(72, '4', 'https://hd.hdbophim.com/20220401/5319_75ef32df/index.m3u8', 8, '2022-10-29 13:34:08', '2022-10-29 13:34:08', NULL),
(73, '3', 'https://hd.hdbophim.com/20220401/5318_5fed8f7f/index.m3u8', 8, '2022-10-29 13:34:08', '2022-10-29 13:34:08', NULL),
(74, '2', 'https://hd.hdbophim.com/20220401/5317_1d5c20cc/index.m3u8', 8, '2022-10-29 13:34:08', '2022-10-29 13:34:08', NULL),
(75, '1', 'https://hd.hdbophim.com/20220401/5316_2019e52d/index.m3u8', 8, '2022-10-29 13:34:08', '2022-10-29 13:34:08', NULL),
(78, 'full', 'https://1080.hdphimonline.com/20220315/2045_2e19a51d/index.m3u8', 20, '2022-11-06 08:33:24', '2022-11-06 08:33:24', NULL),
(79, 'Full', 'https://1080.hdphimonline.com/20220314/1908_c9835323/index.m3u8', 9, '2022-11-06 08:36:05', '2022-11-06 08:36:05', NULL),
(80, 'Full', 'https://aa.nguonphimmoi.com/20220218/401_37f0a03d/index.m3u8', 10, '2022-11-06 08:36:29', '2022-11-06 08:36:29', NULL),
(81, 'Full', 'https://hd.hdbophim.com/20220224/132_ba2b05fd/index.m3u8', 11, '2022-11-06 08:36:58', '2022-11-06 08:36:58', NULL),
(82, 'Full', 'https://hd.hdbophim.com/20220224/133_043e9b97/index.m3u8', 12, '2022-11-06 08:37:28', '2022-11-06 08:37:28', NULL),
(83, 'Full', 'http://aa.nguonphimmoi.com/20220217/221_1cd7bef3/index.m3u8', 13, '2022-11-06 08:37:58', '2022-11-06 08:37:58', NULL),
(84, 'Full', 'https://hd.hdbophim.com/20220224/134_6026c426/index.m3u8', 14, '2022-11-06 08:38:21', '2022-11-06 08:38:21', NULL),
(85, 'Full', 'http://aa.nguonphimmoi.com/20220217/222_856155f2/index.m3u8', 15, '2022-11-06 08:40:48', '2022-11-06 08:40:48', NULL),
(86, 'Full', 'https://1080.hdphimonline.com/20220313/1792_c18238d6/index.m3u8', 16, '2022-11-06 08:41:29', '2022-11-06 08:41:29', NULL),
(87, 'Full', 'https://1080.hdphimonline.com/20220315/2042_aa34b483/index.m3u8', 17, '2022-11-06 08:43:15', '2022-11-06 08:43:15', NULL),
(88, 'Full', 'https://1080.hdphimonline.com/20220315/2043_1b0edafb/index.m3u8', 18, '2022-11-06 08:43:59', '2022-11-06 08:43:59', NULL),
(89, 'Full', 'https://1080.hdphimonline.com/20220315/2044_9b1bc26b/index.m3u8', 19, '2022-11-06 08:44:25', '2022-11-06 08:44:25', NULL),
(90, 'Full', 'https://hd.hdbophim.com/20221116/26040_614bf23c/index.m3u8', 21, '2022-11-17 04:30:32', '2022-11-17 04:30:32', NULL),
(91, 'Full', 'https://1080.hdphimonline.com/20221104/37195_cd82e205/index.m3u8', 22, '2022-11-17 04:35:13', '2022-11-17 04:35:13', NULL),
(92, 'Full', 'https://1080.hdphimonline.com/20220315/2079_861644e1/index.m3u8', 23, '2022-11-17 04:39:30', '2022-11-17 04:39:30', NULL),
(93, 'Full', 'https://1080.hdphimonline.com/20220314/1941_168942bf/index.m3u8', 24, '2022-11-17 04:46:00', '2022-11-17 04:46:00', NULL),
(94, 'Full', 'https://1080.hdphimonline.com/20220314/1940_c82a101e/index.m3u8', 25, '2022-11-17 04:49:40', '2022-11-17 04:49:40', NULL),
(95, 'Full', 'https://kd.hd-bophim.com/20220907/21178_fd1ce83b/index.m3u8', 26, '2022-11-17 04:53:47', '2022-11-17 04:53:47', NULL),
(96, 'Full', 'https://1080.hdphimonline.com/20220311/1519_60a56d65/index.m3u8', 27, '2022-11-17 05:00:21', '2022-11-17 05:00:21', NULL),
(97, 'Full', 'https://1080.hdphimonline.com/20220311/1521_947ebf74/index.m3u8', 28, '2022-11-17 05:03:44', '2022-11-17 05:03:44', NULL),
(98, 'Full', 'https://1080.hdphimonline.com/20220311/1522_367f7974/index.m3u8', 29, '2022-11-17 05:08:40', '2022-11-17 05:08:40', NULL),
(99, 'Full', 'https://1080.hdphimonline.com/20220311/1512_5a0e872e/index.m3u8', 30, '2022-11-17 05:13:21', '2022-11-17 05:13:21', NULL),
(100, 'Full', 'https://1080.hdphimonline.com/20220311/1516_7576ec34/index.m3u8', 31, '2022-11-17 05:16:05', '2022-11-17 05:16:05', NULL),
(101, 'Full', 'https://1080.hdphimonline.com/20220311/1514_2ed491f8/index.m3u8', 32, '2022-11-17 05:19:42', '2022-11-17 05:19:42', NULL),
(102, 'Full', 'https://1080.hdphimonline.com/20220311/1510_3e0fc5c2/index.m3u8', 33, '2022-11-17 05:24:29', '2022-11-17 05:24:29', NULL),
(103, 'Full', 'https://1080.hdphimonline.com/20220311/1511_925f413d/index.m3u8', 34, '2022-11-17 05:30:07', '2022-11-17 05:30:07', NULL),
(104, 'Full', 'https://hd.1080phim.com/20220321/1661_0f5bb4d4/index.m3u8', 35, '2022-11-17 05:36:16', '2022-11-17 05:36:16', NULL),
(105, 'Full', 'https://hd.hdbophim.com/20221116/26062_e55ff6ac/index.m3u8', 36, '2022-11-17 06:21:53', '2022-11-17 06:21:53', NULL),
(106, 'Full', 'https://1080.hdphimonline.com/20220315/2052_271291b2/index.m3u8', 37, '2022-11-20 06:45:49', '2022-11-20 06:45:49', NULL),
(107, 'Full', 'https://hd.1080phim.com/20220409/3072_ffcc46b6/index.m3u8', 38, '2022-11-20 06:49:24', '2022-11-20 06:49:24', NULL),
(108, 'Full', 'https://1080.hdphimonline.com/20220315/2090_03b96925/index.m3u8', 39, '2022-11-20 06:53:33', '2022-11-20 06:53:33', NULL),
(109, 'Full', 'https://1080.hdphimonline.com/20220314/1866_8e486269/index.m3u8', 40, '2022-11-20 06:56:46', '2022-11-20 06:56:46', NULL),
(110, 'Full', 'https://1080.hdphimonline.com/20220314/1865_7944c3fc/index.m3u8', 41, '2022-11-20 07:00:04', '2022-11-20 07:00:04', NULL),
(111, 'Full', 'https://1080.hdphimonline.com/20220314/1861_b3b7cf74/index.m3u8', 42, '2022-11-20 07:04:17', '2022-11-20 07:04:17', NULL),
(112, 'Full', 'https://1080.hdphimonline.com/20220314/1858_e198d3b9/1000k/hls/mixed.m3u8', 43, '2022-11-20 07:08:12', '2022-11-20 07:08:12', NULL),
(113, 'Full', 'https://1080.hdphimonline.com/20220315/2091_f5759fe4/index.m3u8', 44, '2022-11-20 07:12:17', '2022-11-20 07:12:17', NULL),
(114, 'Full', 'https://1080.hdphimonline.com/20220315/2010_3ded1139/index.m3u8', 45, '2022-11-20 07:20:26', '2022-11-20 07:20:26', NULL),
(115, 'Full', 'https://1080.hdphimonline.com/20220315/2041_d2cdb135/index.m3u8', 46, '2022-11-20 07:27:48', '2022-11-20 07:27:48', NULL),
(116, 'Full', 'https://1080.hdphimonline.com/20220315/2089_621bce2f/index.m3u8', 47, '2022-11-20 07:36:31', '2022-11-20 07:36:31', NULL),
(117, 'Full', 'https://1080.hdphimonline.com/20220314/1867_a3fad25d/index.m3u8', 48, '2022-11-20 07:41:13', '2022-11-20 07:41:13', NULL),
(118, 'Full', 'https://1080.hdphimonline.com/20220314/1856_1f94f84f/index.m3u8', 49, '2022-11-20 07:43:58', '2022-11-20 07:43:58', NULL),
(119, 'Full', 'https://1080.hdphimonline.com/20220314/1857_d2efa022/1000k/hls/mixed.m3u8', 50, '2022-11-20 07:51:38', '2022-11-20 07:51:38', NULL),
(120, 'Full', 'https://1080.hdphimonline.com/20220314/1953_9f98d410/index.m3u8', 51, '2022-11-20 07:56:18', '2022-11-20 07:56:18', NULL),
(121, 'Full', 'https://1080.hdphimonline.com/20220314/1887_eac49246/index.m3u8', 52, '2022-11-20 08:01:39', '2022-11-20 08:01:39', NULL),
(122, '8', 'https://hd.1080phim.com/20221204/29352_9809eebd/index.m3u8', 53, '2022-12-09 19:51:03', '2022-12-09 19:51:03', NULL),
(123, '7', 'https://1080.hdphimonline.com/20221203/39091_9324c57b/index.m3u8', 53, '2022-12-09 19:51:03', '2022-12-09 19:51:03', NULL),
(124, '6', 'https://hd.hdbophim.com/20221127/26471_f005dedd/index.m3u8', 53, '2022-12-09 19:51:03', '2022-12-09 19:51:03', NULL),
(125, '5', 'https://hd.1080phim.com/20221126/28450_c39d280e/index.m3u8', 53, '2022-12-09 19:51:03', '2022-12-09 19:51:03', NULL),
(126, '4', 'https://hd.1080phim.com/20221125/28399_dc41351f/index.m3u8', 53, '2022-12-09 19:51:03', '2022-12-09 19:51:03', NULL),
(127, '3', 'https://hd.1080phim.com/20221121/28312_61361641/index.m3u8', 53, '2022-12-09 19:51:03', '2022-12-09 19:51:03', NULL),
(128, '2', 'https://hd.hdbophim.com/20221119/26196_5eb682dc/index.m3u8', 53, '2022-12-09 19:51:03', '2022-12-09 19:51:03', NULL),
(129, '1', 'https://1080.hdphimonline.com/20221118/37564_3cede456/index.m3u8', 53, '2022-12-09 19:51:03', '2022-12-09 19:51:03', NULL),
(130, '8-end', 'https://1080.hdphimonline.com/20221124/37727_577ee97d/index.m3u8', 54, '2022-12-09 20:45:46', '2022-12-09 20:45:46', NULL),
(131, '7', 'https://1080.hdphimonline.com/20221124/37726_9e8ada89/index.m3u8', 54, '2022-12-09 20:45:46', '2022-12-09 20:45:46', NULL),
(132, '6', 'https://1080.hdphimonline.com/20221124/37725_a67ed180/index.m3u8', 54, '2022-12-09 20:45:46', '2022-12-09 20:45:46', NULL),
(133, '5', 'https://1080.hdphimonline.com/20221124/37724_15c6fb51/index.m3u8', 54, '2022-12-09 20:45:46', '2022-12-09 20:45:46', NULL),
(134, '4', 'https://1080.hdphimonline.com/20221124/37723_6c44a1b1/index.m3u8', 54, '2022-12-09 20:45:46', '2022-12-09 20:45:46', NULL),
(135, '3', 'https://1080.hdphimonline.com/20221124/37722_1578c5ce/index.m3u8', 54, '2022-12-09 20:45:46', '2022-12-09 20:45:46', NULL),
(136, '2', 'https://1080.hdphimonline.com/20221124/37721_6009c2e9/index.m3u8', 54, '2022-12-09 20:45:46', '2022-12-09 20:45:46', NULL),
(137, '1', 'https://1080.hdphimonline.com/20221124/37720_3b34fd07/index.m3u8', 54, '2022-12-09 20:45:46', '2022-12-09 20:45:46', NULL),
(138, '9', 'https://hd.1080phim.com/20221209/29825_5762173c/index.m3u8', 53, '2022-12-10 18:41:08', '2022-12-10 18:41:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_genre`
--

CREATE TABLE `tb_genre` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_genre`
--

INSERT INTO `tb_genre` (`id`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Hành Động', 'hanh-dong', '2022-10-29 10:50:15', '2022-10-29 10:50:15', NULL),
(2, 'Phiêu Lưu', 'phieu-luu', '2022-10-29 10:50:35', '2022-10-29 10:50:35', NULL),
(3, 'Kinh Dị', 'kinh-di', '2022-10-29 10:50:44', '2022-10-29 10:50:44', NULL),
(4, 'Tình Cảm', 'tinh-cam', '2022-10-29 10:50:50', '2022-10-29 10:50:50', NULL),
(5, 'Hoạt Hình', 'hoat-hinh', '2022-10-29 10:51:01', '2022-10-29 10:51:01', NULL),
(6, 'Võ Thuật', 'vo-thuat', '2022-10-29 10:51:09', '2022-10-29 10:51:09', NULL),
(7, 'Hài Hước', 'hai-huoc', '2022-10-29 10:51:16', '2022-10-29 10:51:16', NULL),
(8, 'Tâm Lý', 'tam-ly', '2022-10-29 10:51:30', '2022-10-29 10:51:30', NULL),
(9, 'Viễn Tưởng', 'vien-tuong', '2022-10-29 10:51:36', '2022-10-29 10:51:36', NULL),
(10, 'Thần Thoại', 'than-thoai', '2022-10-29 10:52:08', '2022-10-29 10:52:08', NULL),
(11, 'Chiến Tranh', 'chien-tranh', '2022-10-29 10:52:42', '2022-10-29 10:52:42', NULL),
(12, 'Cổ Trang', 'co-trang', '2022-10-29 10:52:49', '2022-10-29 10:52:49', NULL),
(13, 'Âm Nhạc', 'am-nhac', '2022-10-29 10:53:00', '2022-10-29 10:53:00', NULL),
(14, 'Hình Sự', 'hinh-su', '2022-10-29 10:53:06', '2022-10-29 10:53:06', NULL),
(15, 'Khoa Học', 'khoa-hoc', '2022-10-29 10:53:12', '2022-10-29 10:53:12', NULL),
(16, 'Tài Liệu', 'tai-lieu', '2022-10-29 10:53:23', '2022-10-29 10:53:23', NULL),
(17, 'Lịch Sử', 'lich-su', '2022-10-29 10:53:28', '2022-10-29 10:53:28', NULL),
(18, 'Gia Đình', 'gia-dinh', '2022-10-29 10:53:36', '2022-10-29 10:53:36', NULL),
(19, 'Thể Thao', 'the-thao', '2022-10-29 10:53:42', '2022-10-29 10:53:42', NULL),
(20, 'Kiếm Hiệp', 'kiem-hiep', '2022-10-29 10:53:58', '2022-10-29 10:53:58', NULL),
(21, 'Kịch Tính', 'kich-tinh', '2022-10-29 10:54:06', '2022-10-29 10:54:06', NULL),
(22, 'Bí Ẩn', 'bi-an', '2022-10-29 10:54:12', '2022-10-29 10:54:12', NULL),
(23, 'Học Đường', 'hoc-duong', '2022-10-29 10:54:22', '2022-10-29 10:54:22', NULL),
(24, 'Trinh Thám', 'trinh-tham', '2022-11-17 06:21:28', '2022-11-17 06:21:28', NULL),
(25, 'Anime', 'anime', '2022-11-20 06:33:17', '2022-11-20 06:33:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_genre_movie`
--

CREATE TABLE `tb_genre_movie` (
  `MovieId` int(11) NOT NULL,
  `GenreId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_genre_movie`
--

INSERT INTO `tb_genre_movie` (`MovieId`, `GenreId`) VALUES
(1, 1),
(1, 2),
(1, 7),
(1, 15),
(1, 21),
(2, 1),
(2, 2),
(2, 9),
(2, 21),
(3, 1),
(3, 2),
(3, 9),
(3, 21),
(4, 1),
(4, 2),
(4, 11),
(4, 14),
(5, 1),
(5, 2),
(5, 11),
(5, 14),
(6, 1),
(6, 2),
(6, 11),
(6, 14),
(7, 1),
(7, 2),
(7, 11),
(7, 14),
(8, 1),
(8, 2),
(8, 11),
(8, 14),
(9, 1),
(9, 2),
(9, 9),
(9, 15),
(10, 1),
(10, 2),
(10, 9),
(10, 15),
(11, 1),
(11, 2),
(11, 9),
(11, 15),
(12, 1),
(12, 2),
(12, 9),
(12, 15),
(13, 1),
(13, 2),
(13, 9),
(13, 15),
(14, 1),
(14, 2),
(14, 9),
(14, 15),
(15, 1),
(15, 2),
(15, 9),
(15, 15),
(16, 1),
(16, 2),
(16, 9),
(16, 15),
(17, 1),
(17, 2),
(17, 5),
(17, 9),
(17, 15),
(17, 18),
(18, 1),
(18, 9),
(18, 15),
(19, 1),
(19, 9),
(19, 15),
(20, 1),
(20, 9),
(20, 15),
(21, 1),
(21, 9),
(21, 15),
(22, 3),
(22, 22),
(23, 1),
(23, 2),
(23, 7),
(23, 9),
(23, 10),
(24, 1),
(24, 2),
(24, 7),
(24, 9),
(24, 10),
(25, 1),
(25, 2),
(25, 7),
(25, 9),
(25, 10),
(26, 1),
(26, 2),
(26, 7),
(26, 9),
(26, 10),
(27, 2),
(27, 9),
(27, 18),
(27, 22),
(28, 2),
(28, 9),
(28, 18),
(28, 22),
(29, 2),
(29, 9),
(29, 18),
(29, 22),
(30, 2),
(30, 9),
(30, 18),
(30, 22),
(31, 2),
(31, 9),
(31, 18),
(31, 22),
(32, 2),
(32, 9),
(32, 18),
(32, 22),
(33, 2),
(33, 9),
(33, 18),
(33, 22),
(34, 2),
(34, 9),
(34, 18),
(34, 22),
(35, 16),
(36, 1),
(36, 2),
(36, 14),
(36, 22),
(36, 24),
(36, 25),
(37, 1),
(37, 2),
(37, 9),
(37, 22),
(38, 1),
(38, 2),
(38, 9),
(38, 15),
(39, 1),
(39, 2),
(39, 7),
(39, 9),
(39, 15),
(40, 1),
(40, 2),
(40, 9),
(40, 15),
(41, 1),
(41, 2),
(41, 9),
(41, 15),
(42, 1),
(42, 2),
(42, 9),
(42, 15),
(43, 1),
(43, 2),
(43, 9),
(43, 15),
(44, 1),
(44, 2),
(44, 7),
(44, 9),
(44, 15),
(45, 1),
(45, 2),
(45, 9),
(45, 15),
(46, 1),
(46, 2),
(46, 9),
(46, 15),
(47, 1),
(47, 2),
(47, 9),
(47, 15),
(48, 1),
(48, 2),
(48, 9),
(48, 15),
(49, 1),
(49, 2),
(49, 9),
(49, 15),
(50, 1),
(50, 2),
(50, 9),
(50, 15),
(51, 1),
(51, 2),
(51, 9),
(51, 15),
(52, 1),
(52, 2),
(52, 9),
(52, 15),
(53, 4),
(53, 8),
(54, 9),
(54, 18),
(54, 22),
(55, 1),
(55, 2),
(55, 7),
(55, 25);

-- --------------------------------------------------------

--
-- Table structure for table `tb_library`
--

CREATE TABLE `tb_library` (
  `id` int(11) NOT NULL,
  `movie_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_library`
--

INSERT INTO `tb_library` (`id`, `movie_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 42, 1, '2022-12-04 05:17:02', '2022-12-04 05:17:02', NULL),
(2, 33, 1, '2022-12-04 05:20:55', '2022-12-04 05:20:55', NULL),
(3, 2, 1, '2022-12-04 14:13:35', '2022-12-04 14:13:35', NULL),
(5, 41, 1, '2022-12-04 14:13:35', '2022-12-04 14:13:35', NULL),
(6, 52, 1, '2022-12-04 14:13:35', '2022-12-04 14:13:35', NULL),
(7, 17, 1, '2022-12-04 14:13:35', '2022-12-04 14:13:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_link`
--

CREATE TABLE `tb_link` (
  `id` int(11) NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `server_id` int(11) NOT NULL,
  `episode_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_link`
--

INSERT INTO `tb_link` (`id`, `link`, `server_id`, `episode_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'http://aa.nguonphimmoi.com/share/bcbe3365e6ac95ea2c0343a2395834dd', 1, 85, '2022-11-18 12:55:22', '2022-11-18 12:55:22', NULL),
(5, 'https://ok.ru/videoembed/4963310766768?autoplay=1', 3, 129, '2022-12-09 19:52:20', '2022-12-09 19:52:20', NULL),
(6, 'https://ok.ru/videoembed/4969097857712?autoplay=1', 3, 128, '2022-12-09 19:53:28', '2022-12-09 19:53:28', NULL),
(7, 'https://ok.ru/videoembed/4987137559216?autoplay=1', 3, 127, '2022-12-09 19:54:43', '2022-12-09 19:54:43', NULL),
(8, 'https://ok.ru/videoembed/4987137493680?autoplay=1', 3, 126, '2022-12-09 19:55:30', '2022-12-09 19:55:30', NULL),
(9, 'https://ok.ru/videoembed/4996477749936?autoplay=1', 3, 125, '2022-12-09 19:56:06', '2022-12-09 19:56:06', NULL),
(10, 'https://ok.ru/videoembed/5023508335280?autoplay=1', 3, 124, '2022-12-09 19:56:49', '2022-12-09 19:56:49', NULL),
(11, 'https://ok.ru/videoembed/5023508269744?autoplay=1', 3, 123, '2022-12-09 19:57:34', '2022-12-09 19:57:34', NULL),
(12, 'https://ok.ru/videoembed/5038518635184?autoplay=1', 3, 122, '2022-12-09 19:58:15', '2022-12-09 19:58:15', NULL),
(13, 'https://ok.ru/videoembed/5044422904496?autoplay=1', 3, 138, '2022-12-10 18:41:32', '2022-12-10 18:41:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_movie`
--

CREATE TABLE `tb_movie` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aka` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viewed` int(11) DEFAULT 0,
  `liked` int(11) DEFAULT 0,
  `searched` int(11) DEFAULT 0,
  `year_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT 1,
  `type_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `new` int(11) DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `slide` tinyint(1) DEFAULT 0,
  `imdb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_movie`
--

INSERT INTO `tb_movie` (`id`, `name`, `slug`, `aka`, `content`, `thumb`, `background`, `viewed`, `liked`, `searched`, `year_id`, `status_id`, `type_id`, `country_id`, `new`, `created_at`, `updated_at`, `deleted_at`, `slide`, `imdb`, `rating`) VALUES
(1, 'Nữ Khổng Lồ Xanh', 'nu-khong-lo-xanh', 'She Hulk: Attorney at Law', '<p>Jennifer Walters điều hướng cuộc sống phức tạp của một luật sư 30 tuổi độc thân, đồng thời cũng là một gã khổng lồ siêu cường cao 6 feet 7 inch màu xanh lá cây.</p>', 'https://drive.google.com/uc?id=1CXh3Q-AMkaPHMaHfNylmHGO9NFA7Tk0I', 'https://drive.google.com/uc?id=1e3LVGNVVZkmN8Nklg6VHG8qW7hbbY7vO', 0, 0, 0, 12, 2, 1, 6, 1, '2022-10-29 11:08:09', '2022-12-05 18:21:22', NULL, 1, 'tt10857160', 5.2),
(2, 'Gia Tộc Rồng', 'gia-toc-rong', 'House of the Dragon', '<p>Là một phần tiền truyện sắp ra mắt của Game of Thrones . Nó được HBO đặt hàng vào tháng 10 năm 2019. Phần tiền truyện quay trở lại khoảng 200 năm xoay quanh sự khởi đầu của sự kết thúc của triều đại toàn năng từng thống trị Bảy Vương quốc Westeros.Phần tiền truyện Trò chơi vương quyền cho thấy triều đại Targaryen đang ở đỉnh cao sức mạnh tuyệt đối, với hơn 15 con rồng dưới ách của chúng. Hầu hết các đế chế - có thật và trong tưởng tượng - đều sụp đổ từ những đỉnh cao như vậy. Trong trường hợp của nhà Targaryens, sự sụp đổ chậm chạp của họ bắt đầu từ gần năm 193 nhiều năm trước các sự kiện của Game of Thrones , khi Vua Viserys Targaryen phá vỡ truyền thống thế kỷ bằng cách đặt tên con gái mình là Rhaenyra người thừa kế ngai vàng. Nhưng khi Viserys sau này có một đứa con trai, cả triều đình đã bị sốc khi Rhaenyra vẫn giữ tư cách là người thừa kế của ông. , và hạt giống của sự chia rẽ gieo rắc xích mích trên toàn cõi</p>', 'https://drive.google.com/uc?id=1kPkyM6GpykBFDcfA_TIMRE6MpsPwGrTL', 'https://drive.google.com/uc?id=1t791r3z_Q-1P3ffyjBR9G3sAltaFo_RB', 0, 0, 0, 12, 2, 1, 6, 2, '2022-10-29 12:49:18', '2022-12-05 14:22:56', NULL, 1, 'tt11198330', 8.5),
(3, 'Chúa Tể Của Những Chiếc Nhẫn: Những Chiến Nhẫn Toàn Năng', 'chua-te-cua-nhung-chiec-nhan-nhung-chien-nhan-toan-nang', 'The Lord of the Rings: The Rings of Power', '<p>CLoạt phim những chiến nhẫn toàn năng này lần đầu tiên đưa lên màn ảnh những truyền thuyết anh hùng trong lịch sử Huyền thoại thứ hai của Trung Địa. Bộ phim sử thi này lấy bối cảnh hàng nghìn năm trước các sự kiện trong \"Người Hobbit\" và \"Chúa tể của những chiếc nhẫn\" của JRR Tolkien và đưa người xem trở lại thời đại mà sức mạnh vĩ đạiđược rèn giũa, các vương quốc vươn lên vinh quang rồi sụp đổ, những anh hùng khó có khả năng bị thử thách, hy vọng bị treo bởi những sợi tơ tốt nhất và kẻ phản diện vĩ đại nhất từng tuôn trào từ ngòi bút của Tolkien, đe dọa bao trùm cả thế giới trong bóng tối. Bắt đầu trong một thời kỳ hòa bình tương đối, bộ phim theo chân một nhóm nhân vật, cả quen thuộc và mới, khi họ đối đầu với sự tái hợp đáng sợ từ lâu của cái ác đến Trung Địa. Từ độ sâu tối tăm nhất của Dãy núi Sương mù, đến những khu rừng hùng vĩ của thủ đô Lindon, đến vương quốc đảo Númenor ngoạn mục, đến những vùng xa nhất của bản đồ, những vương quốc và nhân vật này đã tạo ra những di sản tồn tại lâu dài sau họ đã biến mất.Nếu bạn nghĩ đây là phim chúa tể của những chiếc nhẫn 4 thì chưa chắc vì đây là phần trước diễn biến của các tập trước.</p>', 'https://drive.google.com/uc?id=1bY-RRhI-H7md_vyzzAFdG7m0xVJPcoC4', 'https://drive.google.com/uc?id=1cqdblO8xmx38GYfra2OpkG8zGnDrBqTK', 0, 0, 0, 12, 2, 1, 6, 3, '2022-10-29 12:59:41', '2022-12-05 13:29:51', NULL, 1, 'tt7631058', 6.9),
(4, 'Phi Vụ Triệu Đô (Phần 1)', 'phi-vu-trieu-do-phan-1', 'Money Heist (Season 1)', '<p><strong>Phi Vụ Triệu Đô (Phần 1)</strong> là một tác phẩm trộm cướp đột phá và cuốn hút đến từ đất nước Tây Ban Nha, La Casa De Papel kể về một nhóm những tội phạm tập hợp lại cho một phi vụ lịch sử: đột nhập Royal Mint of Spain – sở đúc tiền hoàng gia của Tây Ban Nha và tiến hành trộm hàng tỷ euro. Không dừng lại ở việc lấy tiền rồi chạy trốn, kế hoạch của gang trộm này còn bao gồm… in thêm tiền, bắt giữ con tin kéo dài đến hàng chục ngày được thiết kế tỉ mẩn và cực kỳ thông minh bởi nhân vật trùm cuối mang tên Profesor (Giáo Sư). Cùng lúc đó, băng tội phạm sẽ phải cân não với lực lượng cảnh sát, thám tử cũng đang cố gắng giải vây cho nhóm con tin bị bắt giữ.</p>', 'https://drive.google.com/uc?id=1u8_ObxPOxwK9mwLyJVLErJ-b97aD_85e', 'https://drive.google.com/uc?id=1_P6wNaeX9pUlEwn-i59RPpfcQeiWzED1', 323, 12, 0, 7, 2, 1, 13, 0, '2022-10-29 13:21:21', '2022-12-03 12:12:57', NULL, 0, 'tt6468322', 8.2),
(5, 'Phi Vụ Triệu Đô (Phần 2)', 'phi-vu-trieu-do-phan-2', 'Money Heist (Season 2)', '<p><strong>Phi Vụ Triệu Đô (Phần 2)</strong> kể về khi cảnh sát sắp lần ra danh tính Giáo sư, hắn không thể liên lạc được với nhóm cướp trong Xưởng in tiền. Cuộc nổi loạn xảy ra và một trong những tên cướp đã bị tóm.</p>', 'https://drive.google.com/uc?id=1joQIhdO-_EPBgeDsO7AlY0sTdcP7hXAk', 'https://drive.google.com/uc?id=1xnHUVsAFjjHRTK26l83vDhMBNFDfKwdB', 62, 13, 0, 8, 2, 1, 13, 0, '2022-10-29 13:25:23', '2022-12-03 12:12:58', NULL, 0, 'tt6468322', 8.2),
(6, 'Phi Vụ Triệu Đô (Phần 3)', 'phi-vu-trieu-do-phan-3', 'Money Heist (Season 3)', '<p><strong>Phi Vụ Triệu Đô (Phần 3)</strong> tiếp tục khi giáo sư cùng cả nhóm tái ngộ để giải cứu Río. Lần này, họ nhắm vào Ngân hàng Tây Ban Nha với một kế hoạch mới đầy táo bạo và mạo hiểm. Cuộc Kháng chiến tiếp tục.</p>', 'https://drive.google.com/uc?id=1oJLqUWcuNu8ogurJ2kpsqG4M-HjSQep8', 'https://drive.google.com/uc?id=1dbHyI3lUA6kDUjFl2nNBx_VwPohNIv4k', 645, 46, 0, 9, 2, 1, 13, 0, '2022-10-29 13:28:18', '2022-11-29 12:11:48', NULL, 0, 'tt6468322', 8.2),
(7, 'Phi Vụ Triệu Đô (Phần 4)', 'phi-vu-trieu-do-phan-4', 'Money Heist (Season 4)', '<p><strong>Phi Vụ Triệu Đô (Phần 4)</strong> kể về bao mạng sống rơi vào hiểm cảnh khi kế hoạch của Giáo sư dần sáng tỏ và cả nhóm phải chống lại kẻ địch từ cả bên trong lẫn bên ngoài Ngân hàng Tây Ban Nha.</p>', 'https://drive.google.com/uc?id=1IkgVdfKVKuI2oE5nNz0h3MirT0ULvr9b', 'https://drive.google.com/uc?id=14uD_MjGtf3sFR54W5GYsetGMjkq3R7jN', 0, 0, 0, 10, 2, 1, 13, 0, '2022-10-29 13:31:20', '2022-12-03 12:12:59', NULL, 0, 'tt6468322', 8.2),
(8, 'Phi Vụ Triệu Đô (Phần 5)', 'phi-vu-trieu-do-phan-5', 'Money Heist (Season 5)', '<p><strong>Phi Vụ Triệu Đô (Phần 5)</strong> này kể về cả nhóm đã ở Ngân hàng Tây Ban Nha hơn 100 giờ và Giáo Sư đang gặp nguy. Tệ hơn nữa, họ sắp phải đối mặt với một kẻ địch mới: quân đội.</p>', 'https://drive.google.com/uc?id=1G9doGOaRxZIyNtYs9n-dC5rNNa081TB1', 'https://drive.google.com/uc?id=1Ps8sUiU4AI-FEBLpsBrtkaNckwSu61MD', 0, 0, 0, 11, 2, 1, 13, 4, '2022-10-29 13:33:39', '2022-12-02 21:11:23', NULL, 1, 'tt6468322', 8.2),
(9, 'Người Nhện: Xa Nhà', 'nguoi-nhen-xa-nha', 'Spider-Man: Far from Home', '<p>Sau các sự kiện của Avengers: Endgame (2019), Người Nhện phải bước lên để đảm nhận các mối đe dọa mới trong một thế giới đã thay đổi mãi mãi.</p>', 'https://drive.google.com/uc?id=1oi24QW0KOiHRpYza7yk7Pkcd8tk4_Db4', 'https://drive.google.com/uc?id=1b_PV3byCgOkh9AuQS9S2fCg9YETxdjku', 0, 0, 0, 9, 2, 2, 6, 0, '2022-11-05 13:23:23', '2022-12-03 12:13:00', NULL, 0, 'tt6320628', 7.4),
(10, 'Người Nhện', 'nguoi-nhen', 'Spider-Man', '<p>Sau khi bị nhện biến đổi gen cắn, cậu thiến niên Peter Parker nhút nhát sử dụng siêu năng lực mới để chống lại bất công và chiến đấu với một siêu ác nhân muốn báo thù.</p>', 'https://drive.google.com/uc?id=1kRoLl5qu2XBVd831mqe8NqF4W8qKFLvt', 'https://drive.google.com/uc?id=19nN1X04CdD5mchEh6bVJgEHK_XJpI5cE', 0, 0, 0, 1, 2, 2, 6, 0, '2022-11-06 03:42:13', '2022-12-03 12:13:01', NULL, 0, 'tt0145487', 7.4),
(11, 'Người Nhện 2', 'nguoi-nhen-2', 'Spider-Man 2', '<p>Mệt mỏi với cuộc sống hai mặt, Peter Parker tạm gác danh tính siêu anh hùng sang một bên. Liệu anh có trở lại khi kẻ xấu nhiều xúc tu đe dọa Thành phố New York hay không?</p>', 'https://drive.google.com/uc?id=1NHXnwOEr4s3vobcqMbsh4pczHbdv0tRx', 'https://drive.google.com/uc?id=1hMyWZfKO04Yqcp_FSkEV8PmuZ-zC-JtC', 0, 0, 0, 1, 2, 2, 6, 0, '2022-11-06 03:43:58', '2022-11-16 15:59:17', NULL, 0, 'tt0316654', 7.4),
(12, 'Người Nhện 3', 'nguoi-nhen-3', 'Spider-Man 3', '<p>Người Nhện bất khả chiến bại phải chiến đấu với một loạt nhân vật phản diện mới trong phần thứ ba của loạt phim phiêu lưu anh hùng bom tấn.</p>', 'https://drive.google.com/uc?id=1CzJ2_yyK0273pq8yUnvQvIuZWg3TikyK', 'https://drive.google.com/uc?id=1c0iuSI2jZztBmeTNOmczulNqPHABWwAw', 0, 0, 0, 1, 2, 2, 6, 0, '2022-11-06 08:09:59', '2022-12-03 12:13:02', NULL, 0, 'tt0413300', 6.3),
(13, 'Người Nhện Siêu Đẳng', 'nguoi-nhen-sieu-dang', 'The Amazing Spider-Man', '<p>Trong bản làm lại của bộ phim siêu anh hùng này, nam sinh cấp ba Peter Parker học cách sử dụng sức mạnh mới của mình và chiến đấu với tên đại ác nhân Thằn Lằn.</p>', 'https://drive.google.com/uc?id=1GmETA1_Pf0wvqwuwxp-Fz3r3vBw88uuE', 'https://drive.google.com/uc?id=14mg3xk5uOgvOb67avwjj11_t61ZABcaR', 0, 0, 0, 2, 2, 2, 6, 0, '2022-11-06 08:11:20', '2022-11-16 15:59:26', NULL, 0, 'tt0948470', 6.9),
(14, 'Người Nhện Siêu Đẳng 2', 'nguoi-nhen-sieu-dang-2', 'The Amazing Spider-Man 2', '<p>Người nhện chiến đấu chống lại Tê Giác và Người Điện đầy sức mạnh trong khi vẫn cố gắng giữ lời hứa để Gwen Stacy tránh xa cuộc sống nguy hiểm của mình.</p>', 'https://drive.google.com/uc?id=1bVWgK0VI819CW-zHEcvHQZmq7jy-emDb', 'https://drive.google.com/uc?id=1efPsZyrskrFxYnf8h8uNerfElaTSTS3g', 0, 0, 0, 4, 2, 2, 6, 0, '2022-11-06 08:16:11', '2022-11-16 15:59:30', NULL, 0, 'tt1872181', 6.6),
(15, 'Người Nhện: Trở Về Nhà', 'nguoi-nhen-tro-ve-nha', 'Spider-Man: Homecoming', '<p>Peter Parker trở lại cuộc sống học sinh trung học thường ngày, đến khi sự xuất hiện của Kền Kền cho cậu cơ hội chứng minh mình là một siêu anh hùng biết giăng lưới.</p>', 'https://drive.google.com/uc?id=1OUw2IDa5hoIAyWBX7K5swWgvQtyJekWI', 'https://drive.google.com/uc?id=14UTYxZbcyD72YzDP7NZM-b3NQ8dNQ9_-', 0, 0, 0, 7, 2, 2, 6, 0, '2022-11-06 08:19:21', '2022-12-09 20:58:35', NULL, 0, 'tt2250912', 7.4),
(16, 'Người Nhện: Không Còn Nhà', 'nguoi-nhen-khong-con-nha', 'Spider-Man: No Way Home', '<p>Bộ phim tiếp nối sự kiện của phần Spider-man: Far From Home khi phản diện Mysterio đã tiết lộ danh tính thực sự của người nhện là Peter Parker, Spiderman đã phải đối diện với nhiều lời khen chê trái chiều đến từ công chúng, việc này không những làm ảnh hưởng đến cuộc sống của Peter mà còn khiến Dì May, cậu bạn Ned và người yêu MJ bị liên lụy. Vì vậy Peter đã tìm đến Doctor Strange để nhờ ông tìm cách giải quyết chuyện này. Tuy nhiên trong quá trình thi triển thuật đảo ngược thời gian, do nhanh mồm nhanh miệng mà Peter đã khiến việc đọc phép gặp lỗi và mở ra những cánh cửa đưa các tên ác nhân ở các vũ trụ khác đến nơi cậu sinh sống. Liệu rằng Peter có thể sửa chữa được lỗi lầm của mình và điều gì đang chờ đợi cậu ở phía trước? Phim bám sát thuyết đa vũ trụ, cho phép tồn tại các chiều không gian song song. Mang đến trải nghiệm tuyệt vời cho khán giả khi thấy những nhân vật mình yêu thích xuất hiện, từ chính diện cho tới phản diện.</p>', 'https://drive.google.com/uc?id=1a9qqQ_l7gJOtRLvAP3kRHFsc7uDxHiyB', 'https://drive.google.com/uc?id=1q4j4SuGTdWOlV6CGtOpF8-sipg1V699n', 0, 0, 0, 11, 2, 2, 6, 5, '2022-11-06 08:21:14', '2022-11-16 15:59:35', NULL, 0, 'tt10872600', 8.3),
(17, 'Người Nhện: Vũ Trụ Mới', 'nguoi-nhen-vu-tru-moi', 'Spider-Man: Into the Spider-Verse', '<p>Miles Teen Morales trở thành người nhện của vũ trụ của mình, và phải tham gia với năm cá nhân chạy bằng nhện từ các khía cạnh khác để ngăn chặn mối đe dọa cho tất cả các thực tế.</p>', 'https://drive.google.com/uc?id=19mgxXKFyenq6lEPw9PMpfjOGAKgkXx20', 'https://drive.google.com/uc?id=1GZE0356BLceWuILS9XeLBhKZyp7rPl-w', 0, 0, 0, 8, 2, 2, 6, 0, '2022-11-06 08:24:43', '2022-12-09 19:58:44', NULL, 0, 'tt4633694', 8.4),
(18, 'Người Sắt', 'nguoi-sat', 'Iron Man', '<p>Tony Stark. Thiên tài, tỷ phú, playboy, nhà tế học. Con trai của nhà phát minh huyền thoại và nhà thầu vũ khí Howard Stark. Khi Tony Stark được chỉ định để đưa ra một bài thuyết trình vũ khí cho một đơn vị Iraq do Lt. Coll James Rhodes, anh ta đã đưa ra một chuyến đi trên dòng kẻ thù. Đi xe mà kết thúc tồi tệ khi Humvee của Stark rằng anh ta đang cưỡi ngựa bị chiến binh địch tấn công. Anh ta sống sót - hầu như không - với một cái rương đầy đạn và một pin xe gắn liền với trái tim anh ta. Để tồn tại, anh ta xuất hiện với một cách để thu nhỏ pin và số liệu cho thấy pin có thể cung cấp năng lượng cho thứ khác. Do đó người đàn ông sắt được sinh ra. Anh ta sử dụng thiết bị nguyên thủy để thoát khỏi hang động ở Iraq. Khi trở về nhà, sau đó anh bắt đầu làm việc để hoàn thiện bộ đồ của người đàn ông sắt. Nhưng người đàn ông được phụ trách các ngành công nghiệp Stark có kế hoạch của riêng mình để chiếm lĩnh công nghệ của Tony cho các vấn đề khác.</p>', 'https://drive.google.com/uc?id=1YtJKckG_Oh7iiExPo1tkFLwwoElL3oBk', 'https://drive.google.com/uc?id=1flanxCfNXc3r1qsos4YLsCxyqIwwoSMA', 0, 0, 0, 1, 2, 2, 6, 0, '2022-11-06 08:26:54', '2022-11-16 15:59:39', NULL, 0, 'tt0371746', 7.9),
(19, 'Người Sắt 2', 'nguoi-sat-2', 'Iron Man 2', '<p>Với thế giới hiện đã nhận thức được cuộc sống kép của mình với tư cách là người sắt siêu anh hùng bọc thép, nhà phát minh tỷ phú Tony Stark phải đối mặt với áp lực từ chính phủ, báo chí, và công chúng để chia sẻ công nghệ của mình với quân đội. Không muốn buông bỏ phát minh của mình, Stark, cùng với Pepper Potts, và James \"Rhodey\" Rhodes ở bên anh ta, phải rèn rèn các liên minh mới - và đối đầu với kẻ thù mạnh mẽ.</p>', 'https://drive.google.com/uc?id=1MKc0yDIb2Gy34OS1N-RlQMrg15UK0Qqj', 'https://drive.google.com/uc?id=1nKAeHsyoTjgHC9lVYRKXb6NLmFAz36AL', 0, 0, 0, 1, 2, 2, 6, 0, '2022-11-06 08:29:38', '2022-11-16 15:59:43', NULL, 0, 'tt1228705', 6.9),
(20, 'Người Sắt 3', 'nguoi-sat-3', 'Iron Man 3', '<p>\"Người Iron Man 3\" của Marvel\'s hố Brash-nhưng Brash-Brilliant Tony Stark / Iron Man chống lại kẻ thù có vị trí của ai đó không biết giới hạn. Khi Stark tìm thấy thế giới cá nhân của mình bị phá hủy tại bàn tay của kẻ thù, anh ta bắt tay vào một nhiệm vụ đau khổ để tìm những người có trách nhiệm. Hành trình này, ở mỗi lượt, sẽ kiểm tra khí phách của mình. Với lưng chống lại bức tường, Stark còn lại để sống sót qua các thiết bị của mình, dựa vào sự khéo léo và bản năng của mình để bảo vệ những người gần gũi nhất với anh ta. Khi anh ta chiến đấu trở lại, Stark phát hiện ra câu trả lời cho câu hỏi đã bí mật ám ảnh anh ta: Người đàn ông có làm bộ đồ hay bộ đồ làm người đàn ông?</p>', 'https://drive.google.com/uc?id=11F8JR207lzqA2yw0BzQwIv0vxJHHcS7d', 'https://drive.google.com/uc?id=1SaIfUnl7e-xdbllMmzjN8_yhf3b7DBh5', 0, 0, 0, 3, 2, 2, 6, 0, '2022-11-06 08:31:52', '2022-11-19 13:36:34', NULL, 0, 'tt1300854', 7.1),
(21, 'Black Adam', 'black-adam', 'Black Adam', '<p>Phim kể về người đàn ông Black Adam đã bị giam cầm sau gần 5000 năm, giờ đây anh được các vị thần Ai Cập bang cho những sức mạnh đầy quyền năng, và từ đó anh đã thoát ra khỏi sự giam cầm. Từ đây, Black Adam đã tận dụng sức mạnh này để bắt đầu một cuộc sống công lý độc nhất trên thế giới hiện đại hóa này. Không những thế anh còn phải đối mặt với những kẻ thù hung ác mới.</p>', 'https://drive.google.com/uc?id=1q2utl6e3ZruJcOvY_Kcq3CpAcBBxUnoI', 'https://drive.google.com/uc?id=1g8O-r1oUWWmQaE1e_gdw2CQMwq3w_6uL', 0, 0, 0, 12, 2, 2, 6, 7, '2022-11-17 04:21:46', '2022-12-03 12:13:03', NULL, 0, 'tt6443346', 7),
(22, 'Cười', 'cuoi', 'Smile', '<p>Sau khi chứng kiến ​​một sự việc kỳ lạ, đau thương liên quan đến một bệnh nhân, Tiến sĩ Rose Cotter (Sosie Bacon) bắt đầu trải qua những điều đáng sợ xảy ra mà cô ấy không thể giải thích. Khi một nỗi kinh hoàng bao trùm bắt đầu chiếm lấy cuộc sống của cô, Rose phải đối mặt với quá khứ rắc rối của mình để tồn tại và thoát khỏi thực tại mới kinh hoàng của mình.</p>', 'https://drive.google.com/uc?id=1GT-dQ7EOsFvWEgdVk75jjJyrBGrlXitK', 'https://drive.google.com/uc?id=1qGUjkSlgvrfiEUPi8WlUSa6rkYVT2Wau', 0, 0, 0, 12, 2, 2, 6, 6, '2022-11-17 04:34:15', '2022-12-03 12:13:04', NULL, 0, 'tt15474916', 6.7),
(23, 'Thần Sấm Thor', 'than-sam-thor', 'Thor', '<p>Thần Thor mạnh mẽ nhưng kiêu ngạo được đưa ra khỏi Asgard để sống giữa những người ở Midgard (Trái đất), nơi anh ta sớm trở thành một trong những người bảo vệ tốt nhất của họ.</p>', 'https://drive.google.com/uc?id=1AJJD_VortxFlVPIQNNZ4764AGXY7xT0r', 'https://drive.google.com/uc?id=1YVNfZYBI4hYUtY6zT7VwocjqNurNI7Pd', 0, 0, 0, 1, 2, 2, 6, 0, '2022-11-17 04:38:12', '2022-11-17 06:49:29', NULL, 0, 'tt0800369', 7),
(24, 'Thần Sấm 2: Thế Giới Bóng Tối', 'than-sam-2-the-gioi-bong-toi', 'Thor: The Dark World', '<p>Khi những yêu tinh bóng tối cố gắng lao vào vũ trụ thành bóng tối, Thor phải dấn thân vào một hành trình nguy hiểm và cá nhân sẽ đoàn tụ anh ta với bác sĩ Jane Foster.</p>', 'https://drive.google.com/uc?id=1fATxtsXeA5gm_zhV69GxzDV9q9D4SmWo', 'https://drive.google.com/uc?id=1F6YfPrADjrrU_E5Wwicrr58hk4cQxx_U', 0, 0, 0, 3, 2, 2, 6, 0, '2022-11-17 04:44:01', '2022-11-17 06:49:29', NULL, 0, 'tt0800369', 7),
(25, 'Thần Sấm 3: Tận Thế Ragnarok', 'than-sam-3-tan-the-ragnarok', 'Thor: Ragnarok', '<p>Bị giam cầm trên hành tinh Sakaar, Thor phải chạy đua với thời gian để trở về Asgard và ngăn Ragnarök, sự hủy diệt của thế giới của Ngài, dưới bàn tay của Villain Linh hồn mạnh mẽ và tàn nhẫn Hela.</p>', 'https://drive.google.com/uc?id=13hZmwYuQSdsuBW1Lwp2i-cYEb6Rz8iFn', 'https://drive.google.com/uc?id=1C3rTH32D0jvqtfiNHgZJAvFLoMjCi5hO', 0, 0, 0, 7, 2, 2, 6, 0, '2022-11-17 04:48:59', '2022-11-17 06:49:29', NULL, 0, 'tt3501632', 7.9),
(26, 'Thần Sấm 4: Tình Yêu Và Sấm Sét', 'than-sam-4-tinh-yeu-va-sam-set', 'Thor: Love and Thunder', '<p>Câu chuyện xoay quanh cô nàng tình cũ của anh chàng Thor là Jane Foster, cô là người phàm trần và hiện đang phát hiện bản thân mắc phải căn bệnh ung thư. Thor vì để cứu Jane Foster nên đã truyền sức mạnh của mình qua cho cô và từ đó Jane Foster dần sứng đáng và có thể cầm được cây búa, còn Thor thì mất hết sức mạnh và truyền lại danh hiệu thần sấm cho Jane Foster. Sau một vài lần sử dụng sức mạnh thần sấm đã giúp cho Jane Foster chữa trị được căn bệnh của mình.</p>', 'https://drive.google.com/uc?id=1qlNkKus0wBxqk_f05Zl0fkWG7VAWtGBA', 'https://drive.google.com/uc?id=1aHkDohjNzp3QiHW_zq5R-J_1KwrC-OS9', 0, 0, 0, 12, 2, 2, 6, 8, '2022-11-17 04:53:07', '2022-11-17 06:49:33', NULL, 0, 'tt10648342', 6.3),
(27, 'Harry Potter và Hòn Đá Phù Thủy', 'harry-potter-va-hon-da-phu-thuy', 'Harry Potter and the Sorcerer\'s Stone', '<p>Harry Potter và Hòn Đá Phù Thủy là bộ phim đầu tiên trong series phim “Harry Potter” được xây dựng dựa trên tiểu thuyết của nhà văn J.K.Rowling. Nhân vật chính của phim, Harry Potter - một cậu bé 11 tuổi sau khi mồ côi cha mẹ đã bị gửi đến nhà dì dượng của mình, gia đình Dursley. Tuy nhiên, cậu bé bị ngược đãi và không hề biết về thân phận thực sự của mình. Vào sinh nhật thứ 11 của Harry, một người lai khổng lồ là Rubeus Hagrid đã đến tìm cậu bé để đưa cậu về trường Pháp Thuật Hogwarts. Lúc này, Harry mới biết được mình là phù thủy và một phần câu chuyện về cha mẹ mình, những người đã bị Voldemort giết hại. Cùng với trí thông minh và lòng dũng cảm, cậu bé đã cùng những người bạn của mình khám phá những điều bí mật nguy hiểm về thế giới của phép thuật...</p>', 'https://drive.google.com/uc?id=1OaueYXIbbxh5kusBzyoDgH6cVDzw8CB2', 'https://drive.google.com/uc?id=11U5SP3EH9xX9ZtddriEVZNBUBWUNJ4Un', 0, 0, 0, 1, 2, 2, 6, 0, '2022-11-17 04:57:57', '2022-11-17 06:49:36', NULL, 0, 'tt0241527', 7.6),
(28, 'Harry Potter Và Phòng Chứa Bí Mật', 'harry-potter-va-phong-chua-bi-mat', 'Harry Potter and the Chamber of Secrets', '<p>Dựa trên tập thứ hai thuộc loạt sách ăn khách của tác giả J.K. Rowling, bộ phim kể lại cuộc sống của cậu bé Harry Potter trong năm học thứ hai tại trường phù thủy Hogwarts. Mong ngóng trở lại ngôi trường thân yêu của Harry bị một sinh vật kỳ lạ phá tan tành. Chú gia tinh Dobby đã mang tới lời cảnh báo về những nguy cơ tiềm ẩn đang chờ cậu và tìm cách ép Harry nghỉ học. Bất chấp điều đó, Harry vẫn trở lại nơi cậu coi là mái ấm thực sự. Tuy nhiên, Hogwarts có nguy cơ bị đóng cửa vĩnh viễn khi một chuỗi sự kiện kỳ lạ bắt đầu xảy ra: Những vệt máu lạ trên tường, học sinh các Nhà lần lượt hóa đá, câu chuyện thần bí và rùng rợn về phòng chứa bí mật... Rồi Harry phát hiện ra mình nghe thấy giọng nói lạ, phát ra từ đâu đó bên trong bức tường...</p>', 'https://drive.google.com/uc?id=1BeOoVZb7nl16hgZDY-70iYHGU7Z2DxCv', 'https://drive.google.com/uc?id=1FTPBnXcLuQ4zhZDNUTMFKB3PzhxaDkxD', 0, 0, 0, 1, 2, 2, 6, 0, '2022-11-17 05:02:42', '2022-11-17 06:49:41', NULL, 0, 'tt0295297', 7.4),
(29, 'Harry Potter Và Tên Tù Nhân Ngục Azkaban', 'harry-potter-va-ten-tu-nhan-nguc-azkaban', 'Harry Potter and the Prisoner of Azkaban', '<p>Harry Potter may mắn sống sót đến tuổi 13, sau nhiều cuộc tấn công của Chúa tể hắc ám. Nhưng hy vọng có một học kỳ yên ổn với Quidditch của cậu đã tiêu tan thành mây khói khi một kẻ điên cuồng giết người hàng loạt vừa thoát khỏi nhà tù Azkaban, với sự lùng sục của những cai tù là giám ngục.Dường như trường Hogwarts là nơi an toàn nhất cho Harry lúc này. Nhưng có phải là sự trùng hợp khi cậu luôn cảm giác có ai đang quan sát mình từ bóng đêm, và những điềm báo của giáo sư Trelawney liệu có chính xác?</p>', 'https://drive.google.com/uc?id=1qaUx7zsVLK5YiX3uOXVa4sg_Z7CtQdiD', 'https://drive.google.com/uc?id=1lwm6tZbmhISZ5MA2HC2SeYIe4_cPWrw-', 0, 0, 0, 1, 2, 2, 6, 0, '2022-11-17 05:06:58', '2022-11-17 06:49:46', NULL, 0, 'tt0304141', 7.9),
(30, 'Harry Potter Và Chiếc Cốc Lửa', 'harry-potter-va-chiec-coc-lua', 'Harry Potter and the Goblet of Fire', '<p>Khi giải Quidditch Thế giới bị cắt ngang bởi những kẻ ủng hộ Chúa tể Voldemort và sự trở lại của Dấu hiệu hắc ám khủng khiếp, Harry ý thức được rõ ràng rằng, Chúa tể Voldemort đang ngày càng mạnh hơn. Và để trở lại thế giới phép thuật, Chúa tể hắc ám cần phải đánh bại kẻ duy nhất sống sót từ lời nguyền chết chóc của hắn - Harry Potter. Vì lẽ đó, khi Harry bị buộc phải bước vào giải đấu Tam Pháp thuật uy tín nhưng nguy hiểm, cậu nhận ra rằng trên cả chiến thắng, cậu phải giữ được mạng sống của mình.</p>', 'https://drive.google.com/uc?id=1PwYz8PzeO_CY5Ae8dCmL295UNHEqNDT3', 'https://drive.google.com/uc?id=1Ob2Btt1w0h5BPudS2oe735TrKkfdXK_m', 0, 0, 0, 1, 2, 2, 6, 0, '2022-11-17 05:12:32', '2022-11-17 06:49:49', NULL, 0, 'tt0330373', 7.7),
(31, 'Harry Potter Và Hội Phượng Hoàng', 'harry-potter-va-hoi-phuong-hoang', 'Harry Potter and the Order of the Phoenix', '<p>Harry tức giận vì bị bỏ rơi ở nhà Dursley trong dịp hè, cậu ngờ rằng Chúa tể hắc ám Voldemort đang tập hợp lực lượng, và vì cậu có nguy cơ bị tấn công, những người Harry luôn coi là bạn đang cố che giấu tung tích cậu. Cuối cùng, sau khi được giải cứu, Harry khám phá ra rằng giáo sư Dumbledore đang tập hợp lại Hội Phượng Hoàng – một đoàn quân bí mật đã được thành lập từ những năm trước nhằm chống lại Chúa tể Voldemort. Tuy nhiên, Bộ Pháp thuật không ủng hộ Hội Phượng Hoàng, những lời bịa đặt nhanh chóng được đăng tải trên Nhật báo Tiên tri – một tờ báo của giới phù thủy, Harry lo ngại rằng rất có khả năng cậu sẽ phải gánh vác trách nhiệm chống lại cái ác một mình.</p>', 'https://drive.google.com/uc?id=1XsClGTm3Vi01eeukKtdSASdoxWZcu-m3', 'https://drive.google.com/uc?id=16vJkrauj2Pr7YdaB7Zf6JKJzW65Ei_Hk', 0, 0, 0, 1, 2, 2, 6, 0, '2022-11-17 05:15:32', '2022-11-17 06:49:53', NULL, 0, 'tt0373889', 7.5),
(32, 'Harry Potter Và Hoàng Tử Lai', 'harry-potter-va-hoang-tu-lai', 'Harry Potter and the Half-Blood Prince', '<p>Năm học thứ sáu của Harry Potter ở Hogwarts hóa ra lại là một năm đầy thú vị. Điều thú vị đầu tiên là việc chào đón một giáo sư mới tại Hogwarts, thầy Horace Slughorn, người dạy cho Harry nhiều điều thú vị trong môn độc dược của thầy Snape hắc ám. Thầy Slughorn còn cho Harry mượn đồ dùng của trường học, và trong số đó có một quyển sách cũ nát thuộc về chủ nhân tên là Hoàng Tử Lai. Quyển sách có nhiều ghi chú nguệch ngoạc ở lề mỗi trang giấy, và đây lại là người khuyên giúp Harry có thể tiến bộ trong pháp thuật của mình. Cũng trong năm học này, thầy Dumbledore mở một lớp học riêng cho Harry. Ở đó, thầy cho Harry xem những kí ức về những bí mật đen tối của quá khứ Voldermort, với hy vọng rằng Harry có thể lợi dụng những điểm yểu của Voldermort để đánh bại hắn trong trận chiến cuối cùng. Draco Malfoy, kẻ thù không đội trời chung của Harry, có nhiều hành động khả nghi, luôn lén lút đi khắp trường. Vì thế, Harry cho rằng Draco là nội gián của Voldermort và quyết tâm tìm hiểu chính xác chuyện gì đang xảy ra. Nhưng giữa những nguy hiểm cận kề của chúa tể hắc ám, những học sinh của trường Hogwarts vẫn có cuộc sống thường nhật: bận rộn với việc học, cũng như các trận cầu Quidditch và đương nhiên là tình yêu đầu đời của tuổi mới lớn.<br>Ron có cô bạn gái mới, Lavender Brown, và Hermione thì hoàn toàn không vui vẻ gì về chuyện này. Tình bạn giữa họ lại rục rặc và như mọi lần, Harry lại là người đứng giữa. Trong lúc đó, Harry cũng đang đối mặt với những lo lắng của mình khi cậu nhận thấy mình đang thầm thương trộm nhớ em gái của cậu bạn thân nhất, Ginny Weasly, người đang hẹn hò với bạn cùng lớp của Harry, Dean Thomas. Thế lực của Voldermort ngày càng lớn mạnh, cuộc chiến chống lại thế lực hắc ám ngày càng đến gần làm cho bộ phim càng kịch tính hơn. Đã bắt đầu có những hy sinh mất mát …</p>', 'https://drive.google.com/uc?id=1rT1I5quM18MdZwqz2-CFSgMTXZUrmn-r', 'https://drive.google.com/uc?id=168Ab5QtqE693lTC_Ncz819F_r_pBhOAU', 0, 0, 0, 1, 2, 2, 6, 0, '2022-11-17 05:18:31', '2022-11-17 06:49:57', NULL, 0, 'tt0417741', 7.6),
(33, 'Harry Potter Và Bảo Bối Tử Thần 1', 'harry-potter-va-bao-boi-tu-than-1', 'Harry Potter and the Deathly Hallows (Part 1)', '<p>Chúa tể Voldemort cùng đám tay chân của hắn đang âm mưu tấn công Harry Potter khi cậu rời khỏi ngôi nhà của gia đình Dursley lần cuối cùng. Ngay trước khi phép bảo vệ cho Harry tại gia đình Dursley hết hiệu lực vào thời điểm Harry bước qua tuổi 17, người của Hội Phượng hoàng đến để hộ tống cậu tới một nơi ở mới an toàn. Mặc dù đã dùng sáu người nguỵ trang thành Harry, Harry thật vẫn bị Voldemort cùng các Tử thần Thực tử phát hiện ra trên đường đi và bị tấn công...</p>', 'https://drive.google.com/uc?id=1-wAxtJicTm85apsT0yYeFQ_XYgM3kSFT', 'https://drive.google.com/uc?id=1Ig518xpQR3-A6IN1jD0vMttmoZTUKhfq', 0, 0, 0, 1, 2, 2, 6, 0, '2022-11-17 05:21:11', '2022-12-04 21:15:06', NULL, 0, 'tt0926084', 7.7),
(34, 'Harry Potter Và Bảo Bối Tử Thần 2', 'harry-potter-va-bao-boi-tu-than-2', 'Harry Potter and the Deathly Hallows (Part 2)', '<p>Kết thúc phần 1 là khi nhóm bạn Harry Potter, Ron và Hermione quay trở lại trường Hogwarts để tìm và tiêu diệt Trường Sinh Linh Giá cuối cùng của Voldemort. Nhưng Voldemort đã phát hiện ra nhiệm vụ của họ, và trận chiến pháp thuật lớn nhất trong lịch sử đã diễn ra ở đây, nơi mà biết bao người ngã xuống, nơi mà tất cả các pháp sư dũng cảm sẵn sàng đánh đổi cuộc sống của mình để tiêu diệt tận cùng thế lực hắc ám. Giáo sư Dumbledore mặc dù đã mất từ tập 6, nhưng ở phần cuối ông vẫn đóng vai trò quan trọng trong việc dẫn đường chỉ lối cho Harry chiến đấu với phù thủy hắc ám mạnh nhất mọi thời đại...</p>', 'https://drive.google.com/uc?id=1GeAxqta_MYbOJEEnwuLVnKyZ6WQnYhUV', 'https://drive.google.com/uc?id=19hy2bS549bCs6_IM5Dd_TffP1lgxmI7q', 0, 0, 0, 1, 2, 2, 6, 0, '2022-11-17 05:28:59', '2022-11-17 06:50:05', NULL, 0, 'tt1201607', 8.1),
(35, 'Harry Potter Kỷ Niệm 20 Năm: Trở Lại Hogwarts', 'harry-potter-ky-niem-20-nam-tro-lai-hogwarts', 'Harry Potter 20th Anniversary: Return to Hogwarts', '<p>Dàn diễn viên của tất cả các phim \"Harry Potter\" tái hợp trong một chương trình đặc biệt hồi tưởng để kỷ niệm ngày ra mắt phần phim đầu tiên, bao gồm các cuộc phỏng vấn và trò chuyện với dàn diễn viên.</p>', 'https://drive.google.com/uc?id=1KmcGkyjRIhh5rWkca5je5djYVSretUdU', 'https://drive.google.com/uc?id=1pIUwz5gvMlODC9G41e9gb5tlJU4rj028', 0, 0, 0, 11, 2, 2, 6, 0, '2022-11-17 05:35:26', '2022-11-17 06:47:55', NULL, 0, 'tt16116174', 8),
(36, 'Thám Tử Lừng Danh Conan 25: Nàng Dâu Halloween', 'tham-tu-lung-danh-conan-25-nang-dau-halloween', 'Detective Conan: The Bride of Halloween', '<p>Shibuya , Tokyo, nhộn nhịp với mùa Halloween. Một đám cưới đang được tổ chức tại Shibuya Hikarie, nơi Sato đang mặc váy cưới. Trong khi Conan và những vị khách được mời khác đang xem, một kẻ tấn công bất ngờ xông vào và Takagi, người đang cố gắng bảo vệ Sato, bị thương. Takagi sống sót và tình hình đã được giải quyết, nhưng trong mắt Sato, hình ảnh của thần chết mà cô đã thấy khi Jinpei Matsuda, người đàn ông cô yêu, đã bị giết trong một loạt vụ đánh bom ba năm trước, phủ lên Của Takagi.Đồng thời, thủ phạm của các vụ đánh bom đã trốn thoát khỏi nhà tù. Rei Furuya / Toru Amuro, một thành viên của cảnh sát an toàn công cộng, đang truy lùng kẻ đã giết bạn cùng lớp của mình, nhưng một kẻ bí ẩn giả dạng đột nhiên xuất hiện và đặt một quả bom cổ vào người.Conan đến thăm nơi trú ẩn dưới lòng đất nơi Amuro đang ẩn náu để giải giáp quả bom, và nghe về một sự cố cách đây ba năm khi cậu và những người bạn cùng lớp đã qua đời của mình từ học viện cảnh sát gặp phải một kẻ đánh bom ảo không xác định tên là \"Plamya\" ở Shibuya. Khi Conan và nhóm của anh ấy điều tra, một bóng đen đáng lo ngại bắt đầu hiện ra trước mặt họ.</p>', 'https://drive.google.com/uc?id=11AtiJlLgjBGFVg9PQpNHjY4wYD6vK1pS', 'https://drive.google.com/uc?id=1smDqkp4mRyYeuQQW7rwAMzOgEOSuix0W', 0, 0, 0, 12, 2, 2, 3, 9, '2022-11-17 06:20:02', '2022-12-03 12:13:05', NULL, 0, 'tt19770970', 6.9),
(37, 'Phù Thuỷ Tối Thượng', 'phu-thuy-toi-thuong', 'Doctor Strange', '<p>\"Bác sĩ kỳ lạ\" của Marvel tuân theo câu chuyện về bác sĩ phẫu thuật thần kinh tài năng Stephen lạ, sau một tai nạn xe hơi bi thảm, phải đặt bản ngã sang một bên và tìm hiểu những bí mật của một thế giới bí ẩn ẩn và kích thước thay thế. Dựa trên làng Greenwich của thành phố New York, bác sĩ kỳ lạ phải hoạt động như một trung gian giữa thế giới thực và những gì nằm ngoài, sử dụng một loạt các khả năng siêu hình và tạo tác để bảo vệ vũ trụ điện ảnh Marvel.</p>', 'https://drive.google.com/uc?id=1Gs-qeb-SWvxlfrofM95orRPWD-OVvctL', 'https://drive.google.com/uc?id=1aQibo7UHZico7qVM5mWY-080kpP6JJtY', 0, 0, 0, 6, 2, 2, 6, 10, '2022-11-20 06:44:13', '2022-12-03 12:18:09', NULL, 0, 'tt1211837', 7.5),
(38, 'Người Kiến', 'nguoi-kien', 'Ant-Man', '<p>Ant-Man kể về câu chuyện của một tên siêu trộm Scott Lang (do Paul Rudd thủ vai), anh ta ăn trộm được một thí nghiệm bí mật của nhà khoa học Hank Pym (do Michael Douglas thủ vai) và khiến anh ta có khả năng thu nhỏ cơ thể với kích thước chỉ bằng con kiến nhưng vẫn giữ được sức mạnh bình thường của mình.</p>', 'https://drive.google.com/uc?id=1_Ke_ohyq0R2r_Vq4vFNjG8aNED-KS_rV', 'https://drive.google.com/uc?id=17ag1B8gkxozIDzSRuo4WnWHOMB2RBx3w', 0, 0, 0, 5, 2, 2, 6, 11, '2022-11-20 06:48:40', '2022-11-23 06:11:08', NULL, 0, 'tt0478970', 7.3),
(39, 'Vệ Binh Dải Ngân Hà', 've-binh-dai-ngan-ha', 'Guardians of the Galaxy', '<p>Sau khi ăn cắp một quả cầu bí ẩn ở xa tầm xa của không gian bên ngoài, Peter Quill từ Trái đất hiện là mục tiêu chính của một kẻ săn lùng do kẻ phản diện được gọi là Ronan the Accuser. Để giúp chiến đấu với Ronan và đội của mình và cứu thiên hà khỏi sức mạnh của mình, Quill tạo ra một đội những anh hùng không gian được gọi là \"những người bảo vệ của thiên hà\" để cứu thiên hà.</p>', 'https://drive.google.com/uc?id=18UG13bHmNj3-1Yn6a6fu93QFJsG4EP8Q', 'https://drive.google.com/uc?id=1bU0LYF8GWHZpW-S1G6q_oYXXDbLBTBLn', 0, 0, 0, 4, 2, 2, 6, 12, '2022-11-20 06:52:46', '2022-11-23 06:11:09', NULL, 0, 'tt2015381', 8),
(40, 'Captain America: Kẻ Báo Thù Đầu Tiên', 'captain-america-ke-bao-thu-dau-tien', 'Captain America: The First Avenger', '<p>Steve Rogers, một người lính quân sự bị từ chối, biến thành Captain America sau khi lấy một liều một \"huyết thanh siêu quân\". Nhưng là Captain America có giá khi anh ta cố gắng hạ gục một người Monger chiến tranh và một tổ chức khủng bố.</p>', 'https://drive.google.com/uc?id=175UQbwBA7SdUXI1NZTq9QYdvQ7_qeU04', 'https://drive.google.com/uc?id=13DPHf4CJADzA6HjUktX74E3CJOB201Vu', 0, 0, 0, 1, 2, 2, 6, 13, '2022-11-20 06:55:54', '2022-11-23 06:11:10', NULL, 0, 'tt0458339', 6.9),
(41, 'Captain America 2: Chiến Binh Mùa Đông', 'captain-america-2-chien-binh-mua-dong', 'Captain America: The Winter Soldier', '<p>Khi Steve Rogers đấu tranh để nắm lấy vai trò của mình trong thế giới hiện đại, anh ta hợp tác với một đại lý Avenger và S.H.I.E.L.D, Black Widow, để chiến đấu với mối đe dọa mới từ lịch sử: một kẻ ám sát được gọi là người lính mùa đông.</p>', 'https://drive.google.com/uc?id=1OyhvMSRZ1W89BvUHUxtDm1AOpzZq2UVO', 'https://drive.google.com/uc?id=1mgXEblMmYkfQvgE8-2ShG1IszrKrdmuO', 0, 0, 0, 4, 2, 2, 6, 14, '2022-11-20 06:59:06', '2022-12-04 21:15:29', NULL, 0, 'tt1843866', 7.8),
(42, 'Biệt Đội Siêu Anh Hùng', 'biet-doi-sieu-anh-hung', 'The Avengers', '<p>Những anh hùng hùng mạnh của Trái đất phải đến với nhau và học cách chiến đấu với tư cách là một đội nếu họ sẽ ngăn chặn Loki tinh nghịch và quân đội người ngoài hành tinh của mình khỏi sự nô lệ nhân loại.</p>', 'https://drive.google.com/uc?id=17wU9TD6S19nhTROKhCGOhohX71FO4Pdz', 'https://drive.google.com/uc?id=18bjuk5Friz1J9qj88sfnCam0Miaq3no2', 0, 0, 0, 2, 2, 2, 6, 15, '2022-11-20 07:03:40', '2022-12-04 21:12:52', NULL, 0, 'tt0848228', 8),
(43, 'Biệt Đội Siêu Anh Hùng 2: Đế Chế Ultron', 'biet-doi-sieu-anh-hung-2-de-che-ultron', 'Avengers: Age of Ultron', '<p>Khi Tony Stark và Bruce Banner cố gắng nhảy-Bắt đầu một chương trình gìn giữ hòa bình không hoạt động được gọi là Ultron, mọi thứ đã sai lầm khủng khiếp và anh hùng hùng mạnh nhất của Trái đất để ngăn chặn Ultron nhân vật phản diện ban hành kế hoạch khủng khiếp của mình.</p>', 'https://drive.google.com/uc?id=1o_20G1kxF3A5eOW8_Ak89uH-1sk2of9z', 'https://drive.google.com/uc?id=1a0GunWS8E7ejYeGIN2a3jCHudGY7Vz_N', 0, 0, 0, 5, 2, 2, 6, 16, '2022-11-20 07:07:14', '2022-12-03 12:13:05', NULL, 0, 'tt2395427', 7.3),
(44, 'Vệ Binh Dải Ngân Hà 2', 've-binh-dai-ngan-ha-2', 'Guardians of the Galaxy Vol. 2', '<p>Sau khi cứu Xandar khỏi cơn thịnh nộ của Ronan, những người bảo vệ hiện được công nhận là anh hùng. Bây giờ đội phải giúp lãnh đạo lãnh chúa của họ (Chris Pratt) phát hiện ra sự thật đằng sau di sản thực sự của mình. Trên đường đi, kẻ thù cũ chuyển sang đồng minh và sự phản bội đang nở rộ. Và những người bảo vệ thấy rằng họ đang chống lại một mối đe dọa mới tàn phá, người ra ngoài để cai trị thiên hà.</p>', 'https://drive.google.com/uc?id=1mC0C0zqK5e-EiVqTBS5pLe7Pm6mIz5-Y', 'https://drive.google.com/uc?id=18rTrbrGaCztNsYXq4lVjG8jjILh3n2Tn', 0, 0, 0, 7, 2, 2, 6, 17, '2022-11-20 07:11:39', '2022-11-23 06:11:15', NULL, 0, 'tt3896198', 7.6),
(45, 'Chiến Binh Báo Đen', 'chien-binh-bao-den', 'Black Panther', '<p>Sau những sự kiện của Captain America: Nội chiến, Hoàng tử T\'challa trở về quê hương về quốc gia Wakanda chuyên khoa học, tiến bộ công nghệ để phục vụ như Vua mới của đất nước mình. Tuy nhiên, T\'challa sớm thấy rằng anh ta bị thách thức vì ngai vàng từ các phe phái trong đất nước của mình. Khi hai kẻ thù âm mưu tiêu diệt Wakanda, anh hùng được gọi là Black Panther phải hợp tác với C.I.A. Đặc vụ Everett K. Ross và các thành viên của Dora Milaje, các lực lượng đặc biệt của Wakandan, để ngăn chặn Wakanda bị kéo vào một cuộc chiến tranh thế giới.</p>', 'https://drive.google.com/uc?id=1eyIvUSeZkV3ErvubX7T4VjvJe-3u8Iax', 'https://drive.google.com/uc?id=1bBge6HPbhuutgFmS076tjBnjh-gob1rd', 0, 0, 0, 8, 2, 2, 6, 18, '2022-11-20 07:19:21', '2022-11-23 06:11:16', NULL, 0, 'tt1825683', 7.3),
(46, 'Người Kiến Và Chiến Binh Ong', 'nguoi-kien-va-chien-binh-ong', 'Ant-Man and the Wasp', '<p>Khi Scott Lang cân bằng là cả siêu anh hùng và một người cha, Hope Van Dyne và Tiến sĩ Hank Pym thể hiện một nhiệm vụ mới khẩn cấp, tìm thấy Ant-Man chiến đấu với Wasp để khám phá những bí mật từ quá khứ của họ.</p>', 'https://drive.google.com/uc?id=1iovENI8JSvg1S7tX6IISIPIw_Wqh-Jxx', 'https://drive.google.com/uc?id=1nwtpaArptv32Tw8xI7skk2sXI0eBjO4Y', 0, 0, 0, 8, 2, 2, 6, 19, '2022-11-20 07:27:09', '2022-11-20 13:15:34', NULL, 0, 'tt5095030', 7),
(47, 'Venom', 'venom', 'Venom', '<p>Một phóng viên thất bại được liên kết với một thực thể người ngoài hành tinh, một trong nhiều Symbiotes đã xâm chiếm trái đất. Nhưng việc có một cách thích trái đất và quyết định bảo vệ nó.</p>', 'https://drive.google.com/uc?id=17lHNrH4YsUvbRmwU0AFOdPhsq5_gVvZD', 'https://drive.google.com/uc?id=1S3guRr96HfJFGDRlIqFE5KzKyN5muwKt', 0, 0, 0, 8, 2, 2, 6, 20, '2022-11-20 07:35:47', '2022-11-23 06:11:17', NULL, 0, 'tt1270797', 6.6),
(48, 'Captain America 3: Nội Chiến Siêu Anh Hùng', 'captain-america-3-noi-chien-sieu-anh-hung', 'Captain America: Civil War', '<p>Sự tham gia chính trị vào vấn đề Avengers gây ra một sự rạn nứt giữa Captain America và Iron Man.</p>', 'https://drive.google.com/uc?id=1VVPYNNHpTayWVDTLt1NcinvRZyovNarK', 'https://drive.google.com/uc?id=1JymxoDTicK6Un-zegqKnmCYA29_TnxQJ', 0, 0, 0, 6, 2, 2, 6, 21, '2022-11-20 07:40:35', '2022-11-23 06:11:18', NULL, 0, 'tt3498820', 7.8),
(49, 'Biệt Đội Siêu Anh Hùng 3: Cuộc Chiến Vô Cực', 'biet-doi-sieu-anh-hung-3-cuoc-chien-vo-cuc', 'Avengers: Infinity War', '<p>The Avengers và các đồng minh của họ phải sẵn sàng hy sinh tất cả trong một nỗ lực để đánh bại Thanos mạnh mẽ trước khi Blitz của sự tàn phá và hủy hoại của anh ta chấm dứt vũ trụ.</p>', 'https://drive.google.com/uc?id=1hm3c_k_MLLZ4iN-EysK7az_ipRT94dEl', 'https://drive.google.com/uc?id=1uqPkfOl3EzScwIIhXcCRxMlrTYxT1jpo', 0, 0, 0, 8, 2, 2, 6, 22, '2022-11-20 07:43:20', '2022-11-23 06:11:19', NULL, 0, 'tt4154756', 8.4),
(50, 'Biệt Đội Siêu Anh Hùng 4: Hồi Kết', 'biet-doi-sieu-anh-hung-4-hoi-ket', 'Avengers: Endgame', '<p>Sau những sự kiện tàn khốc của Avengers: Infinity War (2018), vũ trụ đang bị hủy hoại. Với sự trợ giúp của các đồng minh còn lại, Avengers đã lắp ráp một lần nữa để đảo ngược hành động của Thanos và khôi phục lại sự cân bằng cho vũ trụ.</p>', 'https://drive.google.com/uc?id=1C8i1IuDQniD8sqp0aHivBjyIxS1CQpXf', 'https://drive.google.com/uc?id=1dObhDs2Nq8nEXyU6xHx6oWm33eVtno44', 0, 0, 0, 9, 2, 2, 6, 23, '2022-11-20 07:50:38', '2022-12-03 12:13:06', NULL, 0, 'tt4154796', 8.4),
(51, 'Đại Úy Marvel', 'dai-uy-marvel', 'Captain Marvel', '<p>Carol Danvers trở thành một trong những anh hùng mạnh nhất của vũ trụ khi Trái đất bị bắt ở giữa một cuộc chiến thiên hà giữa hai chủng tộc ngoài hành tinh.</p>', 'https://drive.google.com/uc?id=1VEcs4AaWr7QZEuBVcTZa13iCuflO3MkA', 'https://drive.google.com/uc?id=1Ldvj5IxvT0scv3EmiDBrtuyrQa6ATmKy', 0, 0, 0, 9, 2, 2, 6, 24, '2022-11-20 07:55:07', '2022-12-03 12:13:07', NULL, 0, 'tt4154664', 6.8),
(52, 'Góa Phụ Đen', 'goa-phu-den', 'Black Widow', '<p>Natasha Romanoff đối mặt với những phần tối hơn của sổ cái của cô ấy khi một âm mưu nguy hiểm với những mối quan hệ với quá khứ của cô.</p>', 'https://drive.google.com/uc?id=12v-DK4WI4NqpkR3aLPn-6VfdHBfoNzYI', 'https://drive.google.com/uc?id=1em29ar6FAeqvZhG4FKpGnRmxOusQVDIx', 0, 0, 0, 11, 2, 2, 6, 25, '2022-11-20 08:00:45', '2022-12-02 21:32:54', NULL, 0, 'tt3480822', 6.7),
(53, 'Cậu Út Nhà Tài Phiệt', 'cau-ut-nha-tai-phiet', 'Reborn Rich', '<p>Cậu Út Nhà Tài Phiệt được chuyển thể từ tiểu thuyết nổi tiếng cùng tên, kể câu chuyện về nhân vật Yoon Hyun Woo - người đã từng làm việc và chịu sự lộng quyền của dòng tộc nhà Chủ tịch Sunyang trong hơn 10 năm. Sau cái chết “thỏ tử cẩu phanh*” có dính líu đến quỹ đen của Chủ tịch, anh ta đã hồi sinh với thân phận Jin Do Joon - con trai út của gia đình của người sáng lập nên tập đoàn Sunyang. Vì kiếp trước đã từng là một kẻ làm thuê nên lần này anh ta sẽ báo thù nhằm nuốt trọn Tập đoàn Sunyang.</p>', 'https://drive.google.com/uc?id=1ltJc7N5XtblajZ1yL_s4PVLjtJHL3xxa', 'https://drive.google.com/uc?id=18vYiv0tLtwI8jb9HwjCYyZTGks6cxZaa', 0, 0, 0, 12, 1, 1, 2, 28, '2022-12-09 19:50:07', '2022-12-18 14:47:12', NULL, 1, 'tt18352538', 8.3),
(54, 'Wednesday', 'wednesday', 'Wednesday', '<p>Wednesday là bộ phim kể về sự thông minh, hay châm chọc và \"chết trong lòng\" một chút, Wednesday Addams điều tra một vụ giết người liên hoàn trong khi có thêm bạn và cả kẻ thù mới ở Học viện Nevermore.</p>', 'https://drive.google.com/uc?id=17s8UsrZVtFSq93DUd9UiuUbLhv5lf5bn', 'https://drive.google.com/uc?id=1gbDvXT1w3tY4K6YXigYkPyY2tQetvQ7l', 0, 0, 0, 12, 2, 1, 6, 27, '2022-12-09 20:44:47', '2022-12-18 14:47:13', NULL, 1, 'tt13443470', 8.4),
(55, 'One Piece Film: Red', 'one-piece-film-red', 'One Piece Film: Red', '<p>Đây là phần phim thứ mười lăm trong loạt phim điện ảnh của One Piece, dựa trên bộ truyện manga nổi tiếng cùng tên của tác giả Eiichiro Oda. Phim được công bố lần đầu tiên vào ngày 21 tháng 11, 2021 để kỷ niệm sự ra mắt của tập phim thứ 1000 của bộ anime One Piece và sau khi tập phim này được phát sóng, đoạn quảng cáo và áp phích chính thức của phim cũng chính thức được công bố. Phim dự kiến sẽ phát hành vào ngày 6 tháng 8 năm 2022. Bộ phim được giới thiệu sẽ là hành trình xoay quanh một nhân vật nữ mới cùng với Shanks \"Tóc Đỏ\".</p>', 'https://drive.google.com/uc?id=1-PXw0nj9s1abn5TT3am5miuia48UX1H3', 'https://drive.google.com/uc?id=1CUj0EYrlKP4AYNBy_J144tfQs5dRn7LR', 0, 0, 0, 12, 2, 2, 3, 0, '2022-12-10 18:56:35', '2022-12-18 14:47:15', NULL, 0, 'tt16183464', 6.9);

-- --------------------------------------------------------

--
-- Table structure for table `tb_role`
--

CREATE TABLE `tb_role` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_role`
--

INSERT INTO `tb_role` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', '2022-10-29 11:03:54', '2022-10-29 11:03:54', NULL),
(2, 'Mod', '2022-10-29 11:04:01', '2022-10-29 11:04:01', NULL),
(3, 'Normal', '2022-10-29 11:04:07', '2022-10-29 11:04:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_room`
--

CREATE TABLE `tb_room` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `master` int(11) DEFAULT NULL,
  `pass` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `private` tinyint(1) DEFAULT NULL,
  `live` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_room`
--

INSERT INTO `tb_room` (`id`, `name`, `master`, `pass`, `private`, `live`, `created_at`, `updated_at`, `deleted_at`, `slug`) VALUES
(5, 'Phòng của khavl741953', 1, NULL, 0, 0, '2022-11-24 08:15:30', '2023-01-10 10:15:13', NULL, 'phong-cua-khavl741953'),
(7, 'Phòng của khavl7419531', 3, NULL, 0, 0, '2022-11-24 08:22:29', '2023-01-10 10:38:18', NULL, 'phong-cua-khavl7419531'),
(9, 'Phòng của ringfake123', 2, '123', 1, 0, '2022-12-03 14:40:06', '2022-12-16 21:02:55', NULL, 'phong-cua-ringfake123'),
(18, 'Phòng của do123', 23, '123', 1, 0, '2023-01-07 20:47:15', '2023-01-08 02:07:57', NULL, 'phong-cua-do123'),
(19, 'Phòng của ringfake1237', 22, NULL, 0, 0, '2023-01-07 21:07:28', '2023-01-07 21:08:26', NULL, 'phong-cua-ringfake1237'),
(20, 'Phòng của scats', 24, '123', 1, 0, '2023-01-07 21:25:01', '2023-01-07 21:28:57', NULL, 'phong-cua-scats'),
(21, 'Phòng của 123', 25, NULL, 0, 0, '2023-01-10 20:18:18', '2023-01-11 11:32:42', NULL, 'phong-cua-123');

-- --------------------------------------------------------

--
-- Table structure for table `tb_server`
--

CREATE TABLE `tb_server` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_server`
--

INSERT INTO `tb_server` (`id`, `name`, `desc`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'OP', 'ophim.cc', '2022-10-29 10:58:56', '2022-10-29 10:58:56', NULL),
(2, 'KS', 'http://kervice.tk/', '2022-11-20 06:17:00', '2022-11-20 06:17:00', NULL),
(3, 'OK', 'ok.ru', '2022-12-09 19:52:09', '2022-12-09 19:52:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_status`
--

CREATE TABLE `tb_status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_status`
--

INSERT INTO `tb_status` (`id`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Đang Tiến Hành', 'dang-tien-hanh', '2022-10-29 11:07:51', '2022-10-29 11:07:51', NULL),
(2, 'Hoàn Thành', 'hoan-thanh', '2022-10-29 11:08:00', '2022-10-29 11:08:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_type`
--

CREATE TABLE `tb_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_type`
--

INSERT INTO `tb_type` (`id`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Phim Bộ', 'phim-bo', '2022-10-29 10:58:35', '2022-10-29 10:58:35', NULL),
(2, 'Phim Lẻ', 'phim-le', '2022-10-29 10:58:42', '2022-10-29 10:58:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'https://drive.google.com/uc?id=1medVHZnNZJj8A_XHEqsPx8cl6dT1MRnu',
  `role_id` int(11) DEFAULT 3,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `refresh_token` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `email`, `username`, `password`, `avatar`, `role_id`, `created_at`, `updated_at`, `deleted_at`, `refresh_token`) VALUES
(1, '', 'khavl741953', '$2b$10$sDL7T6LNLtbSmpQ2mvH59.LzpnSEeQ1uZqaeGJUSrSR0Jb0Uwllnm', 'https://drive.google.com/uc?id=1aCdwx7bQB8Vb394UCiDyv_OmGStZ1eao', 1, '2022-10-29 11:04:14', '2023-01-10 10:36:04', NULL, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJraGF2bDc0MTk1MyIsImVtYWlsIjoiIiwiYXZhdGFyIjoiaHR0cHM6Ly9kcml2ZS5nb29nbGUuY29tL3VjP2lkPTFhQ2R3eDdiUUI4VmIzOTRVQ2lEeXZfT21HU3RaMWVhbyIsInJvbGUiOiJBZG1pbiIsImlhdCI6MTY3MzMyMTc2NCwiZXhwIjoxNjczMzQ2OTY0fQ.vDo72uDb3sjov3KtfDW17A3s95coaJvCIa1akInPwio'),
(2, 'ringfake123@gmail.com', 'ringfake123', '$2b$10$jE/ZiHfGbEsrPZYt.EBHkOzD7RAxvG4hZiQTkZxH.cTdebTA1yO36', 'https://drive.google.com/uc?id=1fhU4HaKV3zLZ0gqVsa9c1_FX3ofn_N9O', 2, '2022-10-29 11:05:09', '2023-01-11 10:38:27', NULL, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidXNlcm5hbWUiOiJyaW5nZmFrZTEyMyIsImVtYWlsIjoicmluZ2Zha2UxMjNAZ21haWwuY29tIiwiYXZhdGFyIjoiaHR0cHM6Ly9kcml2ZS5nb29nbGUuY29tL3VjP2lkPTFST2RqQjBhcC1zRXlLekhBNVlIYTFyZVEzTkFUTVQyOCIsInJvbGUiOiJNb2QiLCJpYXQiOjE2NzM0MDgxNDIsImV4cCI6MTY3MzQzMzM0Mn0.aJTI909SBElrkLDZsYs5v4FhxrlruH51ec3NGJtxYPI'),
(3, 'khavl741953@gmail.com', 'khavl7419531', '$2b$10$bI.cRxS0KlRWv3PXj9fPPeqHuLzjNbyMcIeox9N0HLYhAtWGTovtW', 'https://drive.google.com/uc?id=1OtZ0WShJpu_DGgn9r346PsG5jksI4vng', 3, '2022-11-24 03:16:52', '2023-01-10 10:34:57', NULL, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MywidXNlcm5hbWUiOiJraGF2bDc0MTk1MzEiLCJlbWFpbCI6ImtoYXZsNzQxOTUzQGdtYWlsLmNvbSIsImF2YXRhciI6Imh0dHBzOi8vZHJpdmUuZ29vZ2xlLmNvbS91Yz9pZD0xT3RaMFdTaEpwdV9ER2duOXIzNDZQc0c1amtzSTR2bmciLCJyb2xlIjoiTm9ybWFsIiwiaWF0IjoxNjczMzIxNjk3LCJleHAiOjE2NzMzNDY4OTd9.qRGaq8KCQiyt5G70GDEwYhgvJEyUZEbkZi2r0VCfKrI'),
(22, 'ring@gmail.com', 'ringfake123', '$2b$10$fZhRz7.1nmiVelKRR7RJDuXqpWVG3OQPgY.0eTgCBunm3/zbj1X7O', 'https://drive.google.com/uc?id=1medVHZnNZJj8A_XHEqsPx8cl6dT1MRnu', 3, '2022-12-16 22:07:05', '2023-01-08 00:55:47', NULL, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjIsInVzZXJuYW1lIjoicmluZ2Zha2UxMjM3IiwiZW1haWwiOiIiLCJhdmF0YXIiOiJodHRwczovL2RyaXZlLmdvb2dsZS5jb20vdWM_aWQ9MW1lZFZIWm5OWkpqOEFfWEhFcXNQeDhjbDZkVDFNUm51Iiwicm9sZSI6Ik5vcm1hbCIsImlhdCI6MTY3MzEwNDk1NiwiZXhwIjoxNjczMTMwMTU2fQ.D-W5KMxgbB8LKRKql-19fkcP4CkaKX00QK9_3YgIUgw'),
(23, 'do@gmail.com', 'do123', '$2b$10$gJSS3B1MZ9.Vn/FhfKdqveovKa4InI1wpnpGUO3m.jQz3K6YlnfcC', 'https://drive.google.com/uc?id=1vml6Jv0OhOEppeJ5dIJP0kGrq6Bw0JRy', 3, '2023-01-07 20:45:29', '2023-01-10 20:14:47', NULL, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjMsInVzZXJuYW1lIjoiZG8xMjMiLCJlbWFpbCI6ImRvQGdtYWlsLmNvbSIsImF2YXRhciI6Imh0dHBzOi8vZHJpdmUuZ29vZ2xlLmNvbS91Yz9pZD0xdm1sNkp2ME9oT0VwcGVKNWRJSlAwa0dycTZCdzBKUnkiLCJyb2xlIjoiTm9ybWFsIiwiaWF0IjoxNjczMzUxODgxLCJleHAiOjE2NzMzNzcwODF9.GXER_0zSopTlFJyJCpmWnso13p2jjrhSjPYRHzS1eCA'),
(24, '', 'scats', '$2b$10$p/R.4ae13Uc/qAYVh5KxsObGjOf5c8HuYlfHXVL1d7mK5wiy8sbK2', 'https://drive.google.com/uc?id=1medVHZnNZJj8A_XHEqsPx8cl6dT1MRnu', 3, '2023-01-07 21:24:37', '2023-01-07 21:24:46', NULL, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjQsInVzZXJuYW1lIjoic2NhdHMiLCJlbWFpbCI6IiIsImF2YXRhciI6Imh0dHBzOi8vZHJpdmUuZ29vZ2xlLmNvbS91Yz9pZD0xbWVkVkhabk5aSmo4QV9YSEVxc1B4OGNsNmRUMU1SbnUiLCJyb2xlIjoiTm9ybWFsIiwiaWF0IjoxNjczMTAxNDg2LCJleHAiOjE2NzMxMjY2ODZ9.UCk_59dDfAJJLO506PPh3AZOWqfsPxzrrT3q49pneLw'),
(25, '1@gmail.com', '123', '$2b$10$ZeyYuwmI3PdbAAgaJ4Y7j.RVFSnBj.FB2iNbHzfT2aZ5LZgpXzlFG', 'https://drive.google.com/uc?id=1medVHZnNZJj8A_XHEqsPx8cl6dT1MRnu', 3, '2023-01-07 21:29:03', '2023-01-11 10:48:31', NULL, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjUsInVzZXJuYW1lIjoiMTIzIiwiZW1haWwiOiIxQGdtYWlsLmNvbSIsImF2YXRhciI6Imh0dHBzOi8vZHJpdmUuZ29vZ2xlLmNvbS91Yz9pZD0xbWVkVkhabk5aSmo4QV9YSEVxc1B4OGNsNmRUMU1SbnUiLCJyb2xlIjoiTm9ybWFsIiwiaWF0IjoxNjczNDA4OTExLCJleHAiOjE2NzM0MzQxMTF9.5kuJxAwIOGmOwjIFZ28Sot49Cwt2DR1J-qNlF9iYUyU'),
(26, 'doraemon@gmail.com', 'do123', '$2b$10$VFyIKvCDiQeBQ0dxgjIZ9eLOIRWASA.ZdO5qZP.Mu1WuG.eZBwkNO', 'https://drive.google.com/uc?id=1medVHZnNZJj8A_XHEqsPx8cl6dT1MRnu', 3, '2023-01-08 02:08:59', '2023-01-10 18:56:41', NULL, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjYsInVzZXJuYW1lIjoiZG8xMjM0IiwiZW1haWwiOiIiLCJhdmF0YXIiOiJodHRwczovL2RyaXZlLmdvb2dsZS5jb20vdWM_aWQ9MW1lZFZIWm5OWkpqOEFfWEhFcXNQeDhjbDZkVDFNUm51Iiwicm9sZSI6Ik5vcm1hbCIsImlhdCI6MTY3MzM1MTc3OCwiZXhwIjoxNjczMzc2OTc4fQ.pWOS8RarubHq2PaFAdqo5hDr2OL9ONtGVLCVLhdAwLQ');

-- --------------------------------------------------------

--
-- Table structure for table `tb_year`
--

CREATE TABLE `tb_year` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_year`
--

INSERT INTO `tb_year` (`id`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Trước 2011', 'truoc-2011', '2022-10-29 10:57:33', '2022-10-29 10:57:33', NULL),
(2, '2012', '2012', '2022-10-29 10:57:39', '2022-10-29 10:57:39', NULL),
(3, '2013', '2013', '2022-10-29 10:57:44', '2022-10-29 10:57:44', NULL),
(4, '2014', '2014', '2022-10-29 10:57:48', '2022-10-29 10:57:48', NULL),
(5, '2015', '2015', '2022-10-29 10:57:52', '2022-10-29 10:57:52', NULL),
(6, '2016', '2016', '2022-10-29 10:57:57', '2022-10-29 10:57:57', NULL),
(7, '2017', '2017', '2022-10-29 10:58:01', '2022-10-29 10:58:01', NULL),
(8, '2018', '2018', '2022-10-29 10:58:06', '2022-10-29 10:58:06', NULL),
(9, '2019', '2019', '2022-10-29 10:58:11', '2022-10-29 10:58:11', NULL),
(10, '2020', '2020', '2022-10-29 10:58:16', '2022-10-29 10:58:16', NULL),
(11, '2021', '2021', '2022-10-29 10:58:21', '2022-10-29 10:58:21', NULL),
(12, '2022', '2022', '2022-10-29 10:58:26', '2022-10-29 10:58:26', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_cmt_reply`
--
ALTER TABLE `tb_cmt_reply`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tb_comment`
--
ALTER TABLE `tb_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `movie_id` (`movie_id`);

--
-- Indexes for table `tb_country`
--
ALTER TABLE `tb_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_episode`
--
ALTER TABLE `tb_episode`
  ADD PRIMARY KEY (`id`),
  ADD KEY `movie_id` (`movie_id`);

--
-- Indexes for table `tb_genre`
--
ALTER TABLE `tb_genre`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_genre_movie`
--
ALTER TABLE `tb_genre_movie`
  ADD PRIMARY KEY (`MovieId`,`GenreId`),
  ADD KEY `GenreId` (`GenreId`);

--
-- Indexes for table `tb_library`
--
ALTER TABLE `tb_library`
  ADD PRIMARY KEY (`id`),
  ADD KEY `movie_id` (`movie_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tb_link`
--
ALTER TABLE `tb_link`
  ADD PRIMARY KEY (`id`),
  ADD KEY `server_id` (`server_id`),
  ADD KEY `episode_id` (`episode_id`);

--
-- Indexes for table `tb_movie`
--
ALTER TABLE `tb_movie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_movie_new` (`new`),
  ADD KEY `year_id` (`year_id`),
  ADD KEY `status_id` (`status_id`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `tb_role`
--
ALTER TABLE `tb_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_room`
--
ALTER TABLE `tb_room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `master` (`master`);

--
-- Indexes for table `tb_server`
--
ALTER TABLE `tb_server`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_status`
--
ALTER TABLE `tb_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_type`
--
ALTER TABLE `tb_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `tb_year`
--
ALTER TABLE `tb_year`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_cmt_reply`
--
ALTER TABLE `tb_cmt_reply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `tb_comment`
--
ALTER TABLE `tb_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `tb_country`
--
ALTER TABLE `tb_country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tb_episode`
--
ALTER TABLE `tb_episode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `tb_genre`
--
ALTER TABLE `tb_genre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tb_library`
--
ALTER TABLE `tb_library`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_link`
--
ALTER TABLE `tb_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tb_movie`
--
ALTER TABLE `tb_movie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `tb_role`
--
ALTER TABLE `tb_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_room`
--
ALTER TABLE `tb_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tb_server`
--
ALTER TABLE `tb_server`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_status`
--
ALTER TABLE `tb_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_type`
--
ALTER TABLE `tb_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tb_year`
--
ALTER TABLE `tb_year`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_cmt_reply`
--
ALTER TABLE `tb_cmt_reply`
  ADD CONSTRAINT `tb_cmt_reply_ibfk_269` FOREIGN KEY (`comment_id`) REFERENCES `tb_comment` (`id`),
  ADD CONSTRAINT `tb_cmt_reply_ibfk_270` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`);

--
-- Constraints for table `tb_comment`
--
ALTER TABLE `tb_comment`
  ADD CONSTRAINT `tb_comment_ibfk_273` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`),
  ADD CONSTRAINT `tb_comment_ibfk_274` FOREIGN KEY (`movie_id`) REFERENCES `tb_movie` (`id`);

--
-- Constraints for table `tb_episode`
--
ALTER TABLE `tb_episode`
  ADD CONSTRAINT `tb_episode_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `tb_movie` (`id`);

--
-- Constraints for table `tb_genre_movie`
--
ALTER TABLE `tb_genre_movie`
  ADD CONSTRAINT `tb_genre_movie_ibfk_1` FOREIGN KEY (`MovieId`) REFERENCES `tb_movie` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_genre_movie_ibfk_2` FOREIGN KEY (`GenreId`) REFERENCES `tb_genre` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_library`
--
ALTER TABLE `tb_library`
  ADD CONSTRAINT `tb_library_ibfk_98` FOREIGN KEY (`movie_id`) REFERENCES `tb_movie` (`id`),
  ADD CONSTRAINT `tb_library_ibfk_99` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`);

--
-- Constraints for table `tb_link`
--
ALTER TABLE `tb_link`
  ADD CONSTRAINT `tb_link_ibfk_845` FOREIGN KEY (`server_id`) REFERENCES `tb_server` (`id`),
  ADD CONSTRAINT `tb_link_ibfk_846` FOREIGN KEY (`episode_id`) REFERENCES `tb_episode` (`id`);

--
-- Constraints for table `tb_movie`
--
ALTER TABLE `tb_movie`
  ADD CONSTRAINT `tb_movie_ibfk_1747` FOREIGN KEY (`year_id`) REFERENCES `tb_year` (`id`),
  ADD CONSTRAINT `tb_movie_ibfk_1748` FOREIGN KEY (`status_id`) REFERENCES `tb_status` (`id`),
  ADD CONSTRAINT `tb_movie_ibfk_1749` FOREIGN KEY (`type_id`) REFERENCES `tb_type` (`id`),
  ADD CONSTRAINT `tb_movie_ibfk_1750` FOREIGN KEY (`country_id`) REFERENCES `tb_country` (`id`);

--
-- Constraints for table `tb_room`
--
ALTER TABLE `tb_room`
  ADD CONSTRAINT `tb_room_ibfk_1` FOREIGN KEY (`master`) REFERENCES `tb_user` (`id`);

--
-- Constraints for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD CONSTRAINT `tb_user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `tb_role` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
