## Scats Movie Web

See the movie together, sync videos, and chat in real time!

# 💻How to run it

Clone it via the command line "git clone https://github.com/Rizone2k/scats-web.git" and run "npm install" to install all packages and dependencies, "npm start" to run it

## 🌐Preview

# Scats_0

![image](./src/assets/img/01.PNG)

# Scats_1

![image](./src/assets/img/02.PNG)

# Scats_2

![image](./src/assets/img/03.PNG)

# Scats_3

![image](./src/assets/img/04.PNG)

# Scats_4

![image](./src/assets/img/05.PNG)

# Scats_5

![image](./src/assets/img/06.PNG)

# Scats_6

![image](./src/assets/img/07.PNG)

# Scats_7

![image](./src/assets/img/08.PNG)

# Scats_8

\*Refer: https://github.com/trananhtuat
