import axios from "axios";
import Cookies from "js-cookie";

const instance = axios.create({
  // baseURL: "http://api.scats.tk/api/v1/",
  // baseURL: "http://192.168.1.6:3000/api/v1/",
  baseURL: "http://169.254.226.9:5550/api/v1/",
  // withCredentials: true,
});

instance.interceptors.request.use(
  (config) => {
    config.headers["Access-Control-Allow-Origin"] = "*";
    config.headers["Access-Control-Allow-Methods"] =
      "GET, PUT, DELETE, PATCH, OPTIONS";
    config.headers["Content-Type"] = "multipart/form-data";
    const token = Cookies.get("access_token");
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default instance;
